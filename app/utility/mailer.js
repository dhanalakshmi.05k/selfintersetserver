var nodemailer = require("nodemailer");
var emailConfig=require('../../config/emailConfig');
var smtpTransport = require('nodemailer-smtp-transport');
function  sendEmail(recevierEmailId,message,res){

    var transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
            user: emailConfig.senderEmailId,
            pass: emailConfig.senderPassword
        }
    }));

    var mailOptions = {
        from:  emailConfig.senderEmailId,
        to: recevierEmailId,
        subject: 'Assigment Notification',
        text: message
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            // res.send(error)
        } else {
            console.log('Email sent: ' + info.response);
            res.send("email sent")
        }
    });

}


module.exports ={
    sendEmail:sendEmail
}