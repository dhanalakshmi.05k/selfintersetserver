/**
 * Created by zendynamix on 20-11-2016.
 */

var fromSchema = require('./fromSchema.js');
var mailer = require('./mailer.js');


module.exports={
    fromSchema:fromSchema,
    mailer:mailer
}
