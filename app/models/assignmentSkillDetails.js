let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let assignmentSkillDetailSchema =  new Schema({
    assignmentSkillDetailName:  {type: String, required: true, unique: true, index: true} ,
    assignmentSkillDetailDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentSkillDetail"});
let assignmentSkillDetail = mongoose.model('assignmentSkillDetail',assignmentSkillDetailSchema);
assignmentSkillDetailSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentSkillDetail;
