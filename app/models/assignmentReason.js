let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentReasonSchema =  new Schema({
    assignmentReasonName:   {type: String, required: true, unique: true, index: true} ,
    assignmentReasonDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentReason"});
let assignmentReason = mongoose.model('assignmentReason',assignmentReasonSchema);
assignmentReasonSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentReason;

