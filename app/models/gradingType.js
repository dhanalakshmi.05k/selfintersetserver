let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let gradingTypeSchema =  new Schema({
    gradingName:  {type: String, required: true, unique: true, index: true} ,
   gradingDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "gradingType"});
let gradingType = mongoose.model('gradingType',gradingTypeSchema);
gradingTypeSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = gradingType;

