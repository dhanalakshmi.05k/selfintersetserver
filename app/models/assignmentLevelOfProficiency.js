let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let assignmentLevelOfProficiencySchema =  new Schema({
    assignmentLevelOfProficiencyName:  {type: String, required: true, unique: true, index: true} ,
    assignmentLevelOfProficiencyDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentLevelOfProficiency"});
let assignmentLevelOfProficiency = mongoose.model('assignmentLevelOfProficiency',assignmentLevelOfProficiencySchema);

assignmentLevelOfProficiencySchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentLevelOfProficiency;




