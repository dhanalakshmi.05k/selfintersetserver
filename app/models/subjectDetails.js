let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let subjectDetailsSchema =  new Schema({
        subjectName:  String,
        subjectDesp:String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "subjectDataDetails"});

let subjectDataDetails = mongoose.model('subjectDataDetails',subjectDetailsSchema);
subjectDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = subjectDataDetails;
