let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentCategorySchema =  new Schema({
    assignmentategoryCode:Number,
    assignmentCategoryName: {type: String, required: true, unique: true, index: true} ,
    assignmentCategoryDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentCategory"});
let assignmentCategory = mongoose.model('assignmentCategory',assignmentCategorySchema);

assignmentCategorySchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentCategory;

