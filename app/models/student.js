let mongoose = require('mongoose'),
    Schema = mongoose.Schema;



let studentSchema =  new Schema({
    studentDetails: {
        studentName: String,
        studentPicture: String,
        studentEmail: String,
        studentAttendance:Number,
        studentPeformance:Number,
        studentPhone: String,
        studentAddress: String,
        studentEmergencyContact: Number,
        studentGovemmentId: Number,
        studentDob: Date,
        studentEngagementDate: Date,
        studentDisengagementDate: Date,
        DisengagementReason: String
    },
    studentCourseDetails: {
        courseId: Number,
        classId: Number,
        sectionId: Number,
        semisterId: Number,
        cumulativeGrade: String,
        finalAutomatedGrade: String,
        finalGrade: String,
        comments: String,
        courseCompletionStatus: String,
        courseEnrollmentDate: Date,
        courseDisenrollmentDate: Date,
        reasonForDisenrollment: String,
        studentGradewhenEnrolled: String
    },
    studentSkill: {
        skillList: [String
        ],
        skillActiveTimeStamp: Date,
        skillInactiveTimeStamp: Date,
        skillInactiveReason: String,
        badgeList: [
            String],
        badgeActiveTimeStamp: Date,
        badgeInactiveTimeStamp: Date,
        badgeInactiveReason: String
    },
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "student"});
let student = mongoose.model('student',studentSchema);
studentSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = student;


