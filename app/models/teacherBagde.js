let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentTeacherBadgeSchema =  new Schema({
    teacherBadgeName: { type: String, required: true, unique: true, index: true },
    teacherBadgeDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "teacherBadge"});
let assignmentTeacherBadge = mongoose.model('teacherBadge',assignmentTeacherBadgeSchema);
assignmentTeacherBadgeSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentTeacherBadge;

