let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentEvaluationSchema =  new Schema({
    assignmentEvaluationName: {type: String, required: true, unique: true, index: true} ,
    assignmentEvaluationDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentEvaluation"});
let assignmentEvaluation = mongoose.model('assignmentEvaluation',assignmentEvaluationSchema);

assignmentEvaluationSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentEvaluation;

