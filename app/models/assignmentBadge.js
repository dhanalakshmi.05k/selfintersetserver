let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentBadgeSchema =  new Schema({
    assignmentBadgeName: { type: String, required: true, unique: true, index: true },
    assignmentBadgeDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentBadge"});
let assignmentBadge = mongoose.model('assignmentBadge',assignmentBadgeSchema);
assignmentBadgeSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentBadge;

