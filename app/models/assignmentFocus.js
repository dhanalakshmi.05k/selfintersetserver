let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentFocusSchema =  new Schema({
    assignmentFocusName: {type: String, required: true, unique: true, index: true} ,
    assignmentFocusDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentFocus"});
let assignmentFocus = mongoose.model('assignmentFocus',assignmentFocusSchema);

assignmentFocusSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentFocus;

