let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let facultySchema =  new Schema({
    facultyDetails: {
        facultyName: String,
        facultyPicture: String,
        facultyEmail: String,
        facultyPhone: String,
        facultyAddress: String,
        facultyEmergencyContact: Number,
        facultyGovemmentId: Number,
        facultyDob: Date,
        facultyRole: String
    },
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "facultyDetails"});
let faculty = mongoose.model('facultyDetails',facultySchema);
facultySchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = faculty;
