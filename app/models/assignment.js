let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assignmentSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentDetails"});
let assignmentInfoDetails = mongoose.model('assignmentDetails',assignmentSchema);
assignmentSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentInfoDetails;