
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let unitInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

},{strict:false},{collection: "unitDetails"});

let unit = mongoose.model('unitDetails',unitInfoSchema);
unitInfoSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
unitInfoSchema.pre('unitDetails', function (next) {
    this.updated = new Date();
    next();
});
unitInfoSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});
module.exports = unit;