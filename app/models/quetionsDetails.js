let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let questionsDetailsSchema =  new Schema({
    assignmentName:String,
    subjectName:String,
    className:String,
    sectionName:String,
    AssignmentQuestion:[{
        question:String,
        questionType:String,
        questionOption:{}
    }],
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "questionDetails"});
let questionDetails = mongoose.model('questionDetails',questionsDetailsSchema);
questionsDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = questionDetails;


