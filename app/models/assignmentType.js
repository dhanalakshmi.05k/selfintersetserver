let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentTypeSchema =  new Schema({
    assignmentTypeName:  {type: String, required: true, unique: true, index: true} ,
   assignmentTypeDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentType"});
let assignmentType = mongoose.model('assignmentType',assignmentTypeSchema);
assignmentTypeSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentType;


