let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let fileUploadSchema =  new Schema({
    imagename:{type:String,require:true,unique: true,index: true},
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "fileUploadDetails"});
let fileUploadDetails = mongoose.model('fileUploadDetails',fileUploadSchema);
fileUploadSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = fileUploadDetails;

