
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let courseInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

},{strict:false},{collection: "course"});

let course = mongoose.model('course',courseInfoSchema);
courseInfoSchema.pre('save', function (next) {
    this.updated = new Date();
    next();
});
courseInfoSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
courseInfoSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});
module.exports = course;