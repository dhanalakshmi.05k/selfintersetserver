let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let assignmentSplitBoardSchema =  new Schema({
    assignmentSplitBoardName:  {type: String, required: true, unique: true, index: true} ,
    assignmentSplitBoardDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentSplitBoard"});
let assignmentSplitBoard= mongoose.model('assignmentSplitBoard',assignmentSplitBoardSchema);
assignmentSplitBoardSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentSplitBoard;

