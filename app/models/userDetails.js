let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let userDetailsSchema =  new Schema({
    userName: String,
    password: String,
    email: String,
    proImage: String,
    token: String,
    status: Boolean,
    role: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "userDetails"});
let userDetails = mongoose.model('userDetails',userDetailsSchema);
userDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = userDetails;
