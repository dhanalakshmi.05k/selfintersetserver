let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let semisterDetailsSchema = new Schema({

    semisterName: {type: String, required: true, unique: true, index: true} ,
    semisterDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "semisterDetails"});
let semisterDetails = mongoose.model('semisterDetails',semisterDetailsSchema);

semisterDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = semisterDetails;