let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let classDetailsSchema =  new Schema({
    className: {type: String, required: true, unique: true, index: true} ,
    classDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "classDetails"});
let classDetails = mongoose.model('classDetails',classDetailsSchema);
classDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = classDetails;

