let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let gradeDetailsSchema =  new Schema({
    gradeName:  {type: String, required: true, unique: true, index: true} ,
    gradeDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "gradeDetails"});
let gradeDetails = mongoose.model('gradeDetails',gradeDetailsSchema);
gradeDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = gradeDetails;
