let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let assignmentWeightSchema =  new Schema({
    assignmentWeightCode:Number,
    assignmentWeightName: {type: String, required: true, unique: true, index: true} ,
    assignmentWeightDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assignmentWeight"});
let assignmentCategory = mongoose.model('assignmentWeight',assignmentWeightSchema);

assignmentWeightSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assignmentCategory;

