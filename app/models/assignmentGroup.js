let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assignmentGroupSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now }

},{strict:false},{collection: "assignmentGroupDetails"});

let assignmentGroupDetails = mongoose.model('assignmentGroupDetails',assignmentGroupSchema);
assignmentGroupSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
assignmentGroupSchema.pre('update', function (next) {
    this.updated = new Date();
    next();
});
assignmentGroupSchema.pre('findOneAndUpdate', function (next) {
    this.updated = new Date();
    next();
});

module.exports = assignmentGroupDetails;