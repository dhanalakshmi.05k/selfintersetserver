let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let habitsOfLearnerSchema =  new Schema({
    habitsOfLearnerName:  {type: String, required: true, unique: true, index: true} ,
    habitsOfLearnerDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "habitsOfLearner"});
let habitsOfLearnerModel = mongoose.model('habitsOfLearner',habitsOfLearnerSchema);
habitsOfLearnerSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = habitsOfLearnerModel;

