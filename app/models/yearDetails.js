let mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let yearDetailsSchema =  new Schema({
    yearName: {type: String, required: true, unique: true, index: true} ,
    yearDesp: String,
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "yearDetails"});
let yearDetails = mongoose.model('yearDetails',yearDetailsSchema);

yearDetailsSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});

module.exports = yearDetails;
