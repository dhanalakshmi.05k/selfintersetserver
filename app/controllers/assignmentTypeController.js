var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentTypeModel = mongoose.model('assignmentType');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentType', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentType = new assignmentTypeModel(req.body);
    assignmentType.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentType/count', function(req, res, next) {
    assignmentTypeModel.count(function(err,assignmentCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: assignmentCount};
        res.send(count);
    });
});
router.get('/allAssignmentType/:start/:range', function(req, res, next) {
    assignmentTypeModel.find( function (err, assignmentTypeModel) {
        if (err) return next(err);
        res.send(assignmentTypeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentTypeById/:assignmentTypeId', function(req, res, next) {
   assignmentTypeModel.find({_id:req.params.assignmentTypeId}, function (err,assignmentTypeModel) {
        if (err) return next(err);
        res.send(assignmentTypeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.post('/editAssignmentTypeById/:assignmentTypeId', function(req, res, next) {
    assignmentTypeModel.findOneAndUpdate({"_id": req.params.assignmentTypeId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/deleteAssignmentType/:assignmentTypeId',function(req, res, next){
    console.log('assignmentTypeId', req.params.assignmentTypeId);
    assignmentTypeModel.remove({"_id":req.params.assignmentTypeId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
router.get('/allAssignmentTypeForDropDown', function(req, res, next) {
    assignmentTypeModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});