var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    studentModel = mongoose.model('student');
var mailerService = require('../utility/mailer.js');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addStudent', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var student = new studentModel(req.body);
    student.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allStudent/count', function(req, res, next) {
    studentModel.count(function(err,studentCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: studentCount};
        res.send(count);
    });
});
router.get('/allStudent', function(req, res, next) {
    studentModel.find( function (err, studentModel) {
        if (err) return next(err);
        res.send(studentModel);
    })
});
router.delete('/deleteStudent/:studentId',function(req, res, next){
    console.log('studentId', req.params.studentId);
    studentModel.remove({"_id":req.params.studentId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/students/:classId', function(req, res, next) {
    studentModel.find({"studentCourseDetails.classId":req.params.classId}, function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    })
});


router.get('/getStudentsByEmailId/:studentEmailId', function(req, res, next) {
    studentModel.find({"studentEmail":req.params.studentEmailId}, function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    })
});

router.get('/students/:classId/:sectionId', function(req, res, next) {
    studentModel.find( { $and: [ {"studentCourseDetails.classId":req.params.classId},
            {"studentCourseDetails.sectionId":req.params.sectionId} ] }, function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    })
});


router.post('/publishEmailToStudents', function(req, res, next) {
    console.log("88888888888888req.body.studentEmail------------")
    console.log(req.body)
    mailerService.sendEmail(req.body,"assignmnent Created",res)


});