var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
module.exports = function (app){
    app.use('/', router);
};
var  passport = require('passport');
router.use(passport.initialize());
router.use(passport.session());

router.get('/auth/outlook',
  passport.authenticate('windowslive', {
    scope: [
      'openid',
      'profile',
      'offline_access',
      'https://outlook.office.com/Mail.Read'
    ]
  })
);

router.get('/auth/outlook/callback',
    passport.authenticate('windowslive', {
    successRedirect : 'http://localhost:5200/mainDashBoard',
    failureRedirect: 'http://localhost:5200/login' })
    );
