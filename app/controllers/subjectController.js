var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    subjectDetailsModel = mongoose.model('subjectDataDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addSubjectDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var subjectDetails = new subjectDetailsModel(req.body);
    subjectDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allSubjectDetails/count', function(req, res, next) {
    subjectDetailsModel.count(function(err,subjectCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: subjectCount};
        res.send(count);
    });
});
router.get('/allSubjectDetails/:start/:range', function(req, res, next) {
    subjectDetailsModel.find( function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.get('/subjectDetailsById/:subjectId', function(req, res, next) {
    subjectDetailsModel.find({_id:req.params.subjectId}, function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editSubjectDetailsById/:subjectId', function(req, res, next) {
    subjectDetailsModel.findOneAndUpdate({"_id": req.params.subjectId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/allSubjectsForDropDown', function(req, res, next) {
    subjectDetailsModel.find(function (err, subjectDetailsModel) {
        if (err) return next(err);
        res.send(subjectDetailsModel);
    })
});

router.delete('/deleteSubjectDetails/:subjectId',function(req, res, next){
    console.log('subjectId', req.params.subjectId);
    subjectDetailsModel.remove({"_id":req.params.subjectId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
