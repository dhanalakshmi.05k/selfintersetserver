var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),

    semisterDetailsModel = mongoose.model('semisterDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

/*router.post('/addSemister', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var semisterDetails = new semisterDetailsModel(req.body);
    semisterDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});*/
router.post('/addsemisterDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var semisterDetails = new semisterDetailsModel(req.body);
    semisterDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allSemister/count', function(req, res, next) {
    semisterDetailsModel.count(function(err,semisterCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: semisterCount};
        res.send(count);
    });
});
router.get('/allSemister/:start/:range', function(req, res, next) {
    semisterDetailsModel.find( function (err, semisterDetailsModel) {
        if (err) return next(err);
        res.send(semisterDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/semisterById/:semisterId', function(req, res, next) {
    semisterDetailsModel.find({_id:req.params.semisterId}, function (err, semisterModel) {
        if (err) return next(err);
        res.send(semisterModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editSemisterById/:semisterId', function(req, res, next) {
    semisterDetailsModel.findOneAndUpdate({"_id": req.params.semisterId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.delete('/deleteSemister/:semisterId',function(req, res, next){
    console.log('semisterId', req.params.semisterId);
    semisterDetailsModel.remove({"_id":req.params.semisterId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/allSemesterForDropDown', function(req, res, next) {
    semisterDetailsModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});