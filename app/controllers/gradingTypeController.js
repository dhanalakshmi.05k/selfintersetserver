var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    gradingTypeModel = mongoose.model('gradingType');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addGradingType', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var gradingType = new gradingTypeModel(req.body);
    gradingType.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allGradingType/count', function(req, res, next) {
    gradingTypeModel.count(function(err,gradingTypeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: gradingTypeCount};
        res.send(count);
    });
});
router.get('/allGradingType/:start/:range', function(req, res, next) {
    gradingTypeModel.find( function (err, gradingTypeModel) {
        if (err) return next(err);
        res.send(gradingTypeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.get('/gradingTypesById/:gradingTypeId', function(req, res, next) {
    gradingTypeModel.find({_id:req.params.gradingTypeId}, function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editGradingTypesById/:gradingTypeId', function(req, res, next) {
    gradingTypeModel.findOneAndUpdate({"_id": req.params.gradingTypeId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.delete('/deleteGradingType/:gradingTypeId',function(req, res, next){
    console.log('gradingTypeId', req.params.gradingTypeId);
    gradingTypeModel.remove({"_id":req.params.gradingTypeId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.get('/allGradingTypesForDropDown', function(req, res, next) {
    gradingTypeModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});