var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
module.exports = function (app){
    app.use('/', router);
};
var  passport = require('passport');
router.use(passport.initialize());
router.use(passport.session());
var sessionHandler = require('./../dao/index');

var userDetailsModel = mongoose.model('userDetails');

router.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));



router.get('/signin/details', sessionHandler.loginCheck, function(req, res) {
    console.log("yes");
    sessionHandler.getCredentialsWithToken(req, function (userEmail, userName, token, proImage) {
        var obj={"email": userEmail,"userName": userName,
            "role":"faculty","proImage": proImage, "token": token,status:false};
        console.log("????userObject")
        console.log(obj)
        saveUserAfterLogin(obj);
        res.send(obj);
    });
});

function saveUserAfterLogin(userDataDetails){

    userDetailsModel.findOne({"email":userDataDetails.email},function(err,result){
        console.log("?????????????result")
        console.log(result)
        if(err)
        {
            console.log(err);
        }

       if(!result) {
            var userDetails = new userDetailsModel(userDataDetails);
            userDetails.save(function(err, result) {
                if (err){
                    console.log('fuelDetailsPost failed: ' + err);
                }
            });

        }
    })




}



// the callback after google has authenticated the user
router.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect : 'http://localhost:5200/#/mainDashBoard',
        failureRedirect : 'http://localhost:5200/login'
    }));

