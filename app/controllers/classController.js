var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    classDetailsModel = mongoose.model('classDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addClassDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var classDetails = new classDetailsModel(req.body);
    classDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allClassDetails/count', function(req, res, next) {
    classDetailsModel.count(function(err,classCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: classCount};
        res.send(count);
    });
});
router.get('/allClassDetails/:start/:range', function(req, res, next) {
classDetailsModel.find( function (err, classDetailsModel) {
    if (err) return next(err);
    res.send(classDetailsModel);
}).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/classDetailsById/:classId', function(req, res, next) {
    classDetailsModel.find({_id:req.params.classId}, function (err, classDetailsModel) {
        if (err) return next(err);
        res.send(classDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});


router.post('/editClassDetails/:Id', function(req, res, next) {
    classDetailsModel.findOneAndUpdate(req.params.id, {$set: req.body}, function (err, classDetailsModel) {
        if (err) return next(err);
        res.send('updated successfully.');
    }); });


router.delete('/deleteClassDetails/:classId',function(req, res, next){
    console.log('classId', req.params.classId);
    classDetailsModel.remove({"_id":req.params.classId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/allClassForDropDown', function(req, res, next) {
    classDetailsModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});