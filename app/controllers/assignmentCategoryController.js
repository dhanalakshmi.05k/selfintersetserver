var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentCategoryModel = mongoose.model('assignmentCategory');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentCategory', function(req, res, next) {
    console.log(req.body)
    var assignmentCategory = new assignmentCategoryModel(req.body);
    assignmentCategory.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentCategory/count', function(req, res, next) {
    assignmentCategoryModel.count(function(err,categoryCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: categoryCount};
        res.send(count);
    });
});
router.get('/allAssignmentCategory/:start/:range', function(req, res, next) {
    assignmentCategoryModel.find( function (err, assignmentCategoryModel) {
        if (err) return next(err);
        res.send(assignmentCategoryModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.get('/assignmentCategoryById/assignmentCategoryId', function(req, res, next) {
    assignmentCategoryModel.find({_id: req.params.assignmentCategoryId} , function (err, assignmentCategoryModel) {
        if (err) return next(err);
        res.send(assignmentCategoryModel);
    });

})
router.delete('/deleteAssignmentCategory/:assignmentCategoryId',function(req, res, next){
    console.log('assignmentCategoryId', req.params.assignmentCategoryId);
    assignmentCategoryModel.remove({"_id":req.params.assignmentCategoryId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.post('/editAssignmentCategoryById/:assignmentCategoryId', function(req, res, next) {
    console.log(req.params.assignmentCategoryId)
    console.log(req.body)

    assignmentCategoryModel.findOneAndUpdate({"_id": req.params.assignmentCategoryId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err)
            }
            else{
                res.send(result)
            }

        })
})


router.get('/allAssignmentCategoryForDropDown', function(req, res, next) {
    assignmentCategoryModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});