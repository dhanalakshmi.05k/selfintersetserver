var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentLevelOfProficiencyModel = mongoose.model('assignmentLevelOfProficiency');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentLevelOfProficiency', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentLevelOfProficiency = new assignmentLevelOfProficiencyModel(req.body);
    assignmentLevelOfProficiency.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentLevelOfProficiency/count', function(req, res, next) {
    assignmentLevelOfProficiencyModel.count(function(err,assignmentLevelCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: assignmentLevelCount};
        res.send(count);
    });
});
router.get('/allAssignmentLevelOfProficiency/:start/:range', function(req, res, next) {
    assignmentLevelOfProficiencyModel.find( function (err, assignmentLevelOfProficiencyModel) {
        if (err) return next(err);
        res.send(assignmentLevelOfProficiencyModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentLevelOfProficiencyById/:assignmentLevelOfProficiencyId', function(req, res, next) {
    assignmentLevelOfProficiencyModel.find({_id:req.params.assignmentLevelOfProficiencyId}, function (err, assignmentLevelOfProficiencyModel) {
        if (err) return next(err);
        res.send(assignmentLevelOfProficiencyModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.delete('/deleteAssignmentLevelOfProficiency/:proficiencyId',function(req, res, next){
    console.log('proficiencyId', req.params.proficiencyId);
    assignmentLevelOfProficiencyModel.remove({"_id":req.params.proficiencyId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.post('/editAssignmentLevelOfProficiencyById/:assignmentLOFId', function(req, res, next) {
    assignmentLevelOfProficiencyModel.findOneAndUpdate({"_id": req.params.assignmentLOFId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.get('/allAssignmentLOPForDropDown', function(req, res, next) {
    assignmentLevelOfProficiencyModel.find(function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});