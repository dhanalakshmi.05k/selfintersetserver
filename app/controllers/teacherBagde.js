var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    teacherBadgeModel = mongoose.model('teacherBadge');

module.exports = function (app){
    app.use('/', router);
};



router.post('/addTeacherBadge', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var teacherBadge = new teacherBadgeModel(req.body);
    teacherBadge.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allTeacherBadge/count', function(req, res, next) {
    teacherBadgeModel.count(function(err,badgeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: badgeCount};
        res.send(count);
    });
});
router.get('/allTeacherBadge/:start/:range', function(req, res, next) {
    teacherBadgeModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});


router.get('/assignmentTeacherBadgeById/:assignmentBadgeId', function(req, res, next) {
    teacherBadgeModel.find({_id:req.params.assignmentBadgeId}, function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editAssignmentTeacherBadgeById/:assignmentBadgeId', function(req, res, next) {
    teacherBadgeModel.findOneAndUpdate({"_id": req.params.assignmentBadgeId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/allAssignmentTeacherBadgeForDropDown', function(req, res, next) {
    teacherBadgeModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});

router.delete('/deleteAssignmentTeacherBadge/:assignmentBadgeId',function(req, res, next){
    console.log('assignmentBadgeId', req.params.assignmentBadgeId);
    teacherBadgeModel.remove({"_id":req.params.assignmentBadgeId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
