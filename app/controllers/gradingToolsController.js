var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),

    gradeDetailsModel = mongoose.model('gradeDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addGradingTool', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var gradeDetails = new gradeDetailsModel(req.body);
    gradeDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
+router.get('/gradingTool/count', function(req, res, next) {
    gradeDetailsModel.count(function(err,gradeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: gradeCount};
        res.send(count);
    });
});
router.get('/gradingTools/:start/:range', function(req, res, next) {
    gradeDetailsModel.find( function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    })
});
router.get('/gradingToolsById/:gradingToolId', function(req, res, next) {
    gradeDetailsModel.find({_id:req.params.gradingToolId}, function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editGradingToolsById/:gradingToolId', function(req, res, next) {
    gradeDetailsModel.findOneAndUpdate({"_id": req.params.gradingToolId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/gradingTool/:gradingToolId',function(req, res, next){
    console.log('gradingToolId', req.params.gradingToolId);
    gradeDetailsModel.remove({"_id":req.params.gradingToolId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/allGradingToolsForDropDown', function(req, res, next) {
    gradeDetailsModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});