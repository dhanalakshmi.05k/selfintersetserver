var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    courseDetailsModel = mongoose.model('course');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/saveCourseDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var courseDetails = new courseDetailsModel(req.body);
    courseDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allCourseDetails', function(req, res, next) {
    courseDetailsModel.find({}, function (err, result) {
        if (err) {
            res.send(err)
            console.log(err.stack)
        } else {
            res.send(result)
        }

    })

})


router.get('/courseDetails/:courseId', function(req, res, next) {
    courseDetailsModel.find({"_id":req.params.courseId}, function (err, courseDetails) {
        if (err) return next(err);
        res.send(courseDetails);
    })
});

