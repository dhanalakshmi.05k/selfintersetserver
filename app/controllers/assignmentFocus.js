var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),

    assignmentFocusModel = mongoose.model('assignmentFocus');

module.exports = function (app){
    app.use('/', router);
};



router.post('/addAssignmentFocus', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var focusModel = new assignmentFocusModel(req.body);
    focusModel.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/assignmentFocus/count', function(req, res, next) {
    assignmentFocusModel.count(function(err,gradeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: gradeCount};
        res.send(count);
    });
});
router.get('/assignmentFocus/:start/:range', function(req, res, next) {
    assignmentFocusModel.find( function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    })
});
router.get('/assignmentFocus/:assignmentFocusId', function(req, res, next) {
    assignmentFocusModel.find({_id:req.params.assignmentFocusId}, function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/assignmentFocus/:assignmentFocusId', function(req, res, next) {
    assignmentFocusModel.findOneAndUpdate({"_id": req.params.assignmentFocusId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/assignmentFocus/:assignmentFocusId',function(req, res, next){
    console.log('gradingToolId', req.params.assignmentFocusId);
    assignmentFocusModel.remove({"_id":req.params.assignmentFocusId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/assignmentFocusForDropDown', function(req, res, next) {
    assignmentFocusModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});