var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentWeightModel = mongoose.model('assignmentWeight');

module.exports = function (app){
    app.use('/', router);
};


router.post('/addAssignmentWeight', function(req, res, next) {

    var assignmentWeight = new assignmentWeightModel(req.body);
    assignmentWeight.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentWeight/count', function(req, res, next) {
    assignmentWeightModel.count(function(err,assignmentCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: assignmentCount};
        res.send(count);
    });
});
router.get('/allAssignmentWeight/:start/:range', function(req, res, next) {
    assignmentWeightModel.find( function (err, assignmentTypeModel) {
        if (err) return next(err);
        res.send(assignmentTypeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentWeightById/:assignmentWeightId', function(req, res, next) {
   assignmentWeightModel.find({_id:req.params.assignmentWeightId}, function (err,assignmentWeightModel) {
        if (err) return next(err);
        res.send(assignmentWeightModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.post('/editAssignmentWeightById/:assignmentWeightId', function(req, res, next) {
    assignmentWeightModel.findOneAndUpdate({"_id": req.params.assignmentWeightId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/deleteAssignmentWeight/:assignmentWeightId',function(req, res, next){
    console.log('assignmentWeightId', req.params.assignmentWeightId);
    assignmentWeightModel.remove({"_id":req.params.assignmentWeightId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});



router.get('/allAssignmentWeightingForDropDown', function(req, res, next) {
    assignmentWeightModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});
