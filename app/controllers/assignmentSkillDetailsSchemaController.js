var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentSkillDetailModel = mongoose.model('assignmentSkillDetail');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentSkillDetail', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentSkillDetail = new assignmentSkillDetailModel(req.body);
    assignmentSkillDetail.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentSkillDetail/count', function(req, res, next) {
    assignmentSkillDetailModel.count(function(err,skillCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: skillCount};
        res.send(count);
    });
});
router.get('/allAssignmentSkillDetail/:start/:range', function(req, res, next) {
    assignmentSkillDetailModel.find( function (err, assignmentSkillDetailModel) {
        if (err) return next(err);
        res.send(assignmentSkillDetailModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentSkillById/:assignmentSkillId', function(req, res, next) {
    assignmentSkillModel.find({_id:req.params.assignmentSkillId}, function (err, assignmentSkillModel) {
        if (err) return next(err);
        res.send(assignmentSkillModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editAssignmentSkillById/:assignmentSkillId', function(req, res, next) {
    assignmentSkillModel.findOneAndUpdate({"_id": req.params.assignmentSkillId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/deleteAssignmentSkillDetail/:skillId',function(req, res, next){
    console.log('skillId', req.params.skillId);
    assignmentSkillDetailModel.remove({"_id":req.params.skillId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
router.get('/allSkillDetailsForDropDown', function(req, res, next) {
    assignmentSkillDetailModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});