var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentSplitBoardModel = mongoose.model('assignmentSplitBoard');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentSplitBoard', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentSplitBoard = new assignmentSplitBoardModel(req.body);
    assignmentSplitBoard.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentSplitBoard/count', function(req, res, next) {
    assignmentSplitBoardModel.count(function(err,splitBadgeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: splitBadgeCount};
        res.send(count);
    });
});
router.get('/allAssignmentSplitBoard/:start/:range', function(req, res, next) {
    assignmentSplitBoardModel.find( function (err, assignmentSplitBoardModel) {
        if (err) return next(err);
        res.send(assignmentSplitBoardModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentSplitBoardById/:splitBoardId', function(req, res, next) {
   assignmentSplitBoardModel.find({_id:req.params.splitBoardId}, function (err, assignmentSplitBoardModel) {
        if (err) return next(err);
        res.send(assignmentSplitBoardModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.post('/editAssignmentSplitBoardById/:splitBoardId', function(req, res, next) {
    assignmentSplitBoardModel.findOneAndUpdate({"_id": req.params.splitBoardId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/deleteAssignmentSplitBoard/:splitBoardId',function(req, res, next){
    console.log('splitBoardId', req.params.splitBoardId);
    assignmentSplitBoardModel.remove({"_id":req.params.splitBoardId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
router.get('/allSpiltDashboardForDropDown', function(req, res, next) {
    assignmentSplitBoardModel.find(function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});