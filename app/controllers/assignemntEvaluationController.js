var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentEvaluationModel = mongoose.model('assignmentEvaluation');

module.exports = function (app){
    app.use('/', router);
};



router.post('/addAssignmentEvaluationMarkBy', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentEvaluation = new assignmentEvaluationModel(req.body);
    assignmentEvaluation.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentEvaluationMarkBy/count', function(req, res, next) {
    assignmentEvaluationModel.count(function(err,badgeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: badgeCount};
        res.send(count);
    });
});
router.get('/allAssignmentEvaluationMarkBy/:start/:range', function(req, res, next) {
    assignmentEvaluationModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});


router.get('/assignmentEvaluationMarkById/:assignmentEvaluationId', function(req, res, next) {
    assignmentEvaluationModel.find({_id:req.params.assignmentEvaluationId}, function (err, assignmentEvaluationModel) {
        if (err) return next(err);
        res.send(assignmentEvaluationModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editAssignmentEvaluationByIdMarkBy/:assignmentEvaluationId', function(req, res, next) {
    assignmentEvaluationModel.findOneAndUpdate({"_id": req.params.assignmentEvaluationId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/allAssignmentEvaluationForDropDownMarkBy', function(req, res, next) {
    assignmentEvaluationModel.find( function (err, assignmentEvaluationModel) {
        if (err) return next(err);
        res.send(assignmentEvaluationModel);
    })
});

router.delete('/deleteAssignmentEvaluationMarkBy/:assignmentEvaluationId',function(req, res, next){
    console.log('assignmentBadgeId', req.params.assignmentEvaluationId);
    assignmentEvaluationModel.remove({"_id":req.params.assignmentEvaluationId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
