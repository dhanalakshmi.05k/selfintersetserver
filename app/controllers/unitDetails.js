var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    unitDetailsModel = mongoose.model('unitDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/saveUnitDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var unitDetails = new unitDetailsModel(req.body);
    unitDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});

router.get('/allUnitDetails', function(req, res, next) {
    unitDetailsModel.find({}, function (err, result) {
        if (err) {
            res.send(err)
            console.log(err.stack)
        } else {
            res.send(result)
        }

    })

})


router.get('/unitDetails/:courseId', function(req, res, next) {
    unitDetailsModel.find({"_id":req.params.courseId}, function (err, courseDetails) {
        if (err) return next(err);
        res.send(courseDetails);
    })
});

