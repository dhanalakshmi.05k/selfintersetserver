var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentModel = mongoose.model('assignmentDetails'),
    assignmentGroupModel = mongoose.model('assignmentGroupDetails');
var server = require('../../serverada');
module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignment', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignment= new assignmentModel(req.body);
    assignment.save(function(err, result) {
        if (err){
            res.send(err);
            res.io.emit("broadcast", "clients");
        }
        res.send(result);
    });
});
router.get('/allAssignment/count', function(req, res, next) {
    assignmentModel.count(function(err,assignmentCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: assignmentCount};
        res.send(count);
    });
});
/*
router.get('/allAssignment/:start/:range', function(req, res, next) {
    assignmentModel.find( function (err, assignmentModel) {
        if (err) return next(err);
        res.send(assignmentModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
*/

router.get('/allAssignment/:start/:range', function(req, res, next) {
    assignmentModel.find( function (err, assignmentModel) {
        if (err) return next(err);
        res.send(assignmentModel);
    })
});


router.get('/allAssignmentByFacultyName/:facultyName', function(req, res, next) {
    assignmentModel.find({facultyName:req.params.facultyName} , function (err, assignmentModel) {
        if (err) return next(err);
        res.send(assignmentModel);
    })
});

router.get('/assignmentById/assignmentId', function(req, res, next) {
    assignmentModel.find({_id:req.params.assignmentId} ,function (err, assignmentModel) {
        if (err) return next(err);
        res.send(assignmentModel);
    })
});
router.delete('/deleteAssignmentById/:assignmentId',function(req, res, next){
    console.log('assignmentId', req.params.assignmentId);
    assignmentModel.remove({"_id":req.params.assignmentId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.post('/updateQuestionsIdsForAssignment/:assignmentId', function(req, res, next) {
    console.log('abcddsaf',req.body)
    assignmentModel.findOneAndUpdate({"_id":req.params.assignmentId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

});

router.post('/updateAssignmentDetailsByAssignmentId/:assignmentId', function(req, res, next) {
    console.log('abcddsaf',req.body)
    assignmentModel.findOneAndUpdate({"_id":req.params.assignmentId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

});

router.post('/updateStudentIdForQuestions/:assignmentId', function(req, res, next) {
    console.log('updateStudentIdForQuestions',req.body)
    assignmentModel.findOneAndUpdate({"_id":req.params.assignmentId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

});


router.get('/sendEmailTostudents', function(req, res, next) {
    console.log('updateStudentIdForQuestions',req.body)
    assignmentModel.findOneAndUpdate({"_id":req.params.assignmentId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

});


router.get('/getAssignment/:assginmentId',function(req, res, next){
    console.log('assginmentId', req.params.assginmentId);
    assignmentModel.find({"_id":req.params.assginmentId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});



router.post('/assignmentGroup', function(req, res, next) {
    console.log(req.body)
    var groupModel= new assignmentGroupModel(req.body);
    groupModel.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });

});

router.get('/assignmentAllGroup', function(req, res, next) {
    assignmentGroupModel.find({},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });

});


router.get('/assignmentGroupDetailsById/:groupId',function(req, res, next){
    console.log('groupId', req.params.groupId);
    assignmentGroupModel.find({"_id":req.params.groupId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});



router.get('/assignmentGroupDetailsById/:groupId',function(req, res, next){
    console.log('groupId', req.params.groupId);
    assignmentGroupModel.find({"_id":req.params.groupId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.get('/assignmentGroupDetailsById/:groupId',function(req, res, next){
    console.log('groupId', req.params.groupId);
    assignmentGroupModel.find({"_id":req.params.groupId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

let    fs = require('fs')
router.get('/getAssignmentFile/:imageName',function(req, res, next){
    console.log('groupId', req.params.imageName);
    //read the image using fs and send the image content back in the response
    fs.readFile('uploads/' + req.params.imageName, function (err, content) {
        if (err) {
            res.writeHead(400, {'Content-type':'text/html'})
            console.log(err);
            res.end("No such image");
        } else {
            //specify the content type in the response will be an image
            res.writeHead(200,{'Content-type':'image/jpg'});
            res.end(content);
        }
    });
});


router.post('/addAssignmentGroup', function(req, res, next) {
    console.log(req.body)
    var groupModelDeatils= new assignmentGroupModel(req.body);
    groupModelDeatils.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });

});

router.post('/addStudentToAssignment', function(req, res, next) {
    console.log('abcddsaf',req.body)
    assignmentModel.findOneAndUpdate({"_id":req.body.assignmentId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

});


