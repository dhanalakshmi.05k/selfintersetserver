var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),

    yearDetailsModel = mongoose.model('yearDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addYearDetails', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var yearDetails = new yearDetailsModel(req.body);
    yearDetails.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allYear/count', function(req, res, next) {
    yearDetailsModel.count(function(err,yaerCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: yaerCount};
        res.send(count);
    });
});
router.get('/allYear/:start/:range', function(req, res, next) {
    yearDetailsModel.find( function (err, yearDetailsModel) {
        if (err) return next(err);
        res.send(yearDetailsModel);
    })
});
router.get('/yearById/:yearID', function(req, res, next) {
    yearDetailsModel.find({_id:req.params.yearID}, function (err, yearDetailsModel) {
        if (err) return next(err);
        res.send(yearDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editYearById/:yearID', function(req, res, next) {
    yearDetailsModel.findOneAndUpdate({"_id": req.params.yearID},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/deleteYear/:yearID',function(req, res, next){
    console.log('yearID', req.params.yearID);
    yearDetailsModel.remove({"_id":req.params.yearID},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/allYearForDropDown', function(req, res, next) {
    yearDetailsModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});