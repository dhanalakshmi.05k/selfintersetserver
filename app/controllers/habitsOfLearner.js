var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),

    habitsOfLearnersModel = mongoose.model('habitsOfLearner');

module.exports = function (app){
    app.use('/', router);
};



router.post('/addHabitsOfLearners', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var learnersModel = new habitsOfLearnersModel(req.body);
    learnersModel.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/habitsOfLearners/count', function(req, res, next) {
    habitsOfLearnersModel.count(function(err,gradeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: gradeCount};
        res.send(count);
    });
});
router.get('/habitsOfLearners/:start/:range', function(req, res, next) {
    habitsOfLearnersModel.find( function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    })
});
router.get('/habitsOfLearners/:habitsOfLearnersId', function(req, res, next) {
    habitsOfLearnersModel.find({_id:req.params.habitsOfLearnersId}, function (err, gradeDetailsModel) {
        if (err) return next(err);
        res.send(gradeDetailsModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/habitsOfLearners/:habitsOfLearnersId', function(req, res, next) {
    habitsOfLearnersModel.findOneAndUpdate({"_id": req.params.habitsOfLearnersId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})
router.delete('/habitsOfLearners/:habitsOfLearnersId',function(req, res, next){
    console.log('gradingToolId', req.params.habitsOfLearnersId);
    habitsOfLearnersModel.remove({"_id":req.params.habitsOfLearnersId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/habitsOfLearnersForDropDown', function(req, res, next) {
    habitsOfLearnersModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});