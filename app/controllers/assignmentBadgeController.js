    var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentBadgeModel = mongoose.model('assignmentBadge');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentBadge', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentBadge = new assignmentBadgeModel(req.body);
    assignmentBadge.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentBadge/count', function(req, res, next) {
    assignmentBadgeModel.count(function(err,badgeCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: badgeCount};
        res.send(count);
    });
});
router.get('/allAssignmentBadge/:start/:range', function(req, res, next) {
    assignmentBadgeModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});


router.get('/assignmentBadgeById/:assignmentBadgeId', function(req, res, next) {
    assignmentBadgeModel.find({_id:req.params.assignmentBadgeId}, function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editAssignmentBadgeById/:assignmentBadgeId', function(req, res, next) {
    assignmentBadgeModel.findOneAndUpdate({"_id": req.params.assignmentBadgeId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/allAssignmentBadgeForDropDown', function(req, res, next) {
        assignmentBadgeModel.find( function (err, assignmentBadgeModel) {
            if (err) return next(err);
            res.send(assignmentBadgeModel);
        })
    });

router.delete('/deleteAssignmentBadge/:assignmentBadgeId',function(req, res, next){
    console.log('assignmentBadgeId', req.params.assignmentBadgeId);
    assignmentBadgeModel.remove({"_id":req.params.assignmentBadgeId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
