var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    assignmentReasonModel = mongoose.model('assignmentReason');

module.exports = function (app){
    app.use('/', router);
};

router.get('/test', function (req, res,next) {
    res.send('Greetings from the Test controller!'); });

router.post('/addAssignmentReason', function(req, res, next) {
    console.log('abcddsaf',req.body)
    var assignmentReason = new assignmentReasonModel(req.body);
    assignmentReason.save(function(err, result) {
        if (err){
            res.send(err);
        }
        res.send(result);
    });
});
router.get('/allAssignmentReason/count', function(req, res, next) {
    assignmentReasonModel.count(function(err,reasonCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: reasonCount};
        res.send(count);
    });
});
router.get('/allAssignmentReason/:start/:range', function(req, res, next) {
    assignmentReasonModel.find( function (err, assignmentReasonModel) {
        if (err) return next(err);
        res.send(assignmentReasonModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});
router.get('/assignmentReasonById/:assignmentReasonId', function(req, res, next) {
    assignmentReasonModel.find({_id:req.params.assignmentReasonId}, function (err, assignmentReasonModel) {
        if (err) return next(err);
        res.send(assignmentReasonModel);
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
});

router.post('/editAssignmentReasonById/:assignmentReasonId', function(req, res, next) {
    assignmentReasonModel.findOneAndUpdate({"_id": req.params.assignmentReasonId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.delete('/deleteAssignmentReason/:reasonId',function(req, res, next){
    console.log('reasonId', req.params.reasonId);
    assignmentReasonModel.remove({"_id":req.params.reasonId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});
router.get('/allAssignmentResonsForDropDown', function(req, res, next) {
    assignmentReasonModel.find( function (err, assignmentBadgeModel) {
        if (err) return next(err);
        res.send(assignmentBadgeModel);
    })
});