var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    facultyDetailsModel = mongoose.model('facultyDetails');

module.exports = function (app){
    app.use('/', router);
};


router.get('/allUsers/count', function(req, res, next) {
    facultyDetailsModel.count(function(err,userCount){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: userCount};
        res.send(count);
    });
})

router.get('/allFacultyDetails', function(req, res, next) {
    facultyDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})






