var express = require('express'),
    config = require('./config/config'),
    glob = require('glob'),
    mongoose = require('mongoose');
multer = require('multer');
var session      = require('express-session');
var RedisStore = require('connect-redis')(session);

mongoose.connect('mongodb://localhost/projectAdA',{ useNewUrlParser: true });


var db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});


var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
    require(model);
});
var app = express();
app.use(session({
    store: new RedisStore({
        host: "127.0.0.1",
        port: "6379"
    }),
    secret: 'dhcommentmoderator',
    saveUninitialized: true,
    resave: true
}));
app.use(multer({dest:'uploads'}).any());
require('./config/express')(app,config);


app.listen(config.port, function () {
    console.log('Express server listening on port ada server ' + config.port);
});





