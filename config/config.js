  var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
          chalk = require('chalk'),
    env = process.env.NODE_ENV || 'development';
  dbHost=process.env.DB_HOST_NAME,
  dbPort=process.env.DB_PORT

var config = {

  development: {
    root: rootPath,
    app: {
      name: 'projectAdA'
    },
    port: 5200,
    db: 'mongodb://localhost/projectAdA',
    PARAMETERS:{
              SESSION_EXPIRE_TIME:'24h' //in seconds
          }

  }
};

module.exports = config['development'];
  logConfiguration();

  function logConfiguration(){

    console.log("\n\n ---------------------Configuration in Use --------------------------")
    console.log(config[env])
  }

