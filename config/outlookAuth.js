const OutlookStrategy = require('passport-outlook').Strategy;

var OUTLOOK_CLIENT_ID = "35be5e47-9054-43e1-a29d-245aa5fc8830";
var OUTLOOK_CLIENT_SECRET = "ogqyDLYM65~+nuqTHF711#(";


passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});



passport.use(new OutlookStrategy({
    clientID: OUTLOOK_CLIENT_ID,
    clientSecret: OUTLOOK_CLIENT_SECRET,
    callbackURL: "http://localhost:5200/auth/outlook/callback"
  },function(accessToken, refreshToken, profile, done) {
       var user = {
         outlookId: profile.id,
         name: profile.DisplayName,
         email: profile.EmailAddress,
         accessToken:  accessToken
       };
       if (refreshToken)
         user.refreshToken = refreshToken;
       if (profile.MailboxGuid)
         user.mailboxGuid = profile.MailboxGuid;
       if (profile.Alias)
         user.alias = profile.Alias;
         User.findOrCreate(user, function (err, user) {
         return done(err, user);
       });
  }
));