const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const OutlookStrategy = require('passport-outlook').Strategy;

var OUTLOOK_CLIENT_ID = "35be5e47-9054-43e1-a29d-245aa5fc8830";
var OUTLOOK_CLIENT_SECRET = "ogqyDLYM65~+nuqTHF711#(";
var OUTLOOK_CALLBACK = "http://localhost:5200/auth/outlook/callback";


module.exports = (passport) => {
    passport.serializeUser((user, done) => {
        done(null, user);
    });
    passport.deserializeUser((user, done) => {
        done(null, user);
    });


    /* clientID:"1058480964559-cd4m4kp852br5iheam8lqps5lj83qkev.apps.googleusercontent.com",
        clientSecret:"HELraZiVehYuKGcAAkPUGEjA",
        callbackURL:"http://localhost:5200/auth/google/callback"

    */
    passport.use(new GoogleStrategy({
        clientID:"256658356863-s5l2gk76v7prc6et9i08hv8c22r3h2r4.apps.googleusercontent.com",
        clientSecret:"_4CKfxtMcC1k_cqD9R4nBD58",
        callbackURL:"http://projectada.in:5200/auth/google/callback"
    },function(token, refreshToken, profile, done) {
            console.log(profile);
            var newUser = {};
            newUser.id = profile.id;
            newUser.token = token;
            newUser.name = profile.displayName;
            newUser.email = profile.emails[0].value;
            newUser.proImage = profile.photos[0].value;
            return done(null, newUser);
        }
    ));

    passport.use(new OutlookStrategy({
      clientID: OUTLOOK_CLIENT_ID,
      clientSecret: OUTLOOK_CLIENT_SECRET,
      callbackURL: OUTLOOK_CALLBACK
    },function(token, refreshToken, profile, done) {
          var newUser = {};
          newUser.id = profile.id;
          newUser.token = token;
          newUser.name = profile.displayName;
          newUser.email = profile.emails[0].value;
          //newUser.google.proImage = profile.photos[0].value;
          //if (refreshToken)
          // newUser.google.refreshToken = refreshToken;
          return done(null, newUser);
    }
));
};