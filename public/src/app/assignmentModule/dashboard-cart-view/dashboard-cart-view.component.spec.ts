import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCartViewComponent } from './dashboard-cart-view.component';

describe('DashboardCartViewComponent', () => {
  let component: DashboardCartViewComponent;
  let fixture: ComponentFixture<DashboardCartViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCartViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
