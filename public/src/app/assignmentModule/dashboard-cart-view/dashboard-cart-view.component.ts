import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import {CookieService} from "angular2-cookie/core";
declare var $: any;

import * as moment from 'moment';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-dashboard-cart-view',
  templateUrl: './dashboard-cart-view.component.html',
  styleUrls: ['./dashboard-cart-view.component.css']
})
export class DashboardCartViewComponent implements OnInit {
  selectedDevice="Assignment"
  showStatusBar=false
  deleteAssignmentIntermediate:any={}
  localIndex:number
  assignmentToDelete:any={}
  recordAvailableStatus=false
  isListViewActive:boolean=false
  currentUser:string;
  isGridViewActive:boolean=false
  assignmentList:any=[]
  constructor(private router: Router,private assignmentServiceService:AssignmentServiceService
    ,public _cookieService:CookieService,private http: HttpClient) {
    if(localStorage.getItem('currentUser') &&JSON.parse(localStorage.getItem('currentUser')).user ){
      this.currentUser=JSON.parse(localStorage.getItem('currentUser')).user
    }

  }

  deleteAssignmentIntermediateDetails(assign){
    this.deleteAssignmentIntermediate=assign
  }


  zoomAdded(assignmnetEditDetailss){
    this.localIndex=assignmnetEditDetailss.index
      $(".listWrap").click(function(){
      $(".listWrap").removeClass("active")

      $(this).addClass("active")
    })
   /*  this.navigateToEditAssignment(assignmnetEditDetailss)*/
  }
  showPopup(){
    $("#pop0").removeClass("DN")
    $("#pop0").addClass("DB")
    $("#createAssignmentPopUp").removeClass("DN")
  }
  ngOnInit() {
   this.showStatusBar= this.assignmentServiceService.getAssginmentStatus()
    this.getAllAssignments(0,10);
   this.getUserDetailsBySession()
  }

  getUserDetailsBySession() {
    console.log("result????????????????????called????")
    this.assignmentServiceService.getUserDetailsSession()
      .subscribe((userDetails: any) => {
        console.log("userDetails>>>>>>>>>>>>>>>>>")
        console.log(userDetails)
        localStorage.setItem('currentUser', JSON.stringify({ user: userDetails['email']}));
        this.currentUser=JSON.parse(localStorage.getItem('currentUser')).user
      },
        error => {
          // Handle result
          console.log("result????????????errorerror????????????")
          console.log(error)
        })
  }





  getAllAssignments(skip,limit){
    this.assignmentServiceService.getAllAssignmentDetailsBYFacultyName(this.currentUser)
    .subscribe((data: any) => {
      for(var j=0;j<data.length;j++){
        var daysDuration ;
        var minutesDuration ;
        var secondDuration ;
        var startDate = moment(data[j].assignmentStartDate); //todays date
        var endDate = moment(data[j].assignmentEndDate); // another date
        var duration = moment.duration(endDate.diff(startDate));

        if(duration.asDays()>1){
           daysDuration = duration.asDays();
        }
        else  if(duration.asDays()<1){
          daysDuration =Math.round( duration.asHours());
        }
        else  if(duration.asHours()<1){
          minutesDuration =Math.round(duration.asMinutes()) ;
        }
        else  if(duration.asMinutes()<1){
          secondDuration =Math.round( duration.asSeconds());
        }

        data[j]['index']=j

        if(daysDuration){
          data[j]['daysDuration']=daysDuration
        }
        if(minutesDuration){
          data[j]['minutesDuration']=minutesDuration
        }
        if(secondDuration){
          data[j]['secondDuration']=secondDuration
        }
      }

      console.log("difence data.reverse() two days=========================")
      console.log(data.reverse())

      this.assignmentList= data.reverse();
    });
  }

  navigateListView(){
    this.isGridViewActive=false
    this.isListViewActive=true
    this.router.navigate(['assignmentListView']);
  }
  navigateAssignment(){
    this.router.navigate(['mainDashBoard']);
  }
  navigateDashBoardView(){
    this.assignmentServiceService.showstatus(false)
    this.isGridViewActive=true
    this.isListViewActive=false
    this.router.navigate(['mainDashBoard']);
  }

  logout(){
    this.router.navigate(['login']);
  }

  showRecordsBasedonType(recordType){
    console.log("************clicekd called"+recordType)
    if(recordType==="all"||recordType==="Assignment"){
      this.recordAvailableStatus=true
      this.getAllAssignments(0,10);
    }
    else{
      this.assignmentList=[]
      this.recordAvailableStatus=false
    }

  }

  navigateToEditAssignment(assignmnentEditDetails){
    console.log(assignmnentEditDetails)
    this.router.navigate(['createAssignment',assignmnentEditDetails['_id']]);

  }

  deleteAssignmentDetails(assignment){

    this.assignmentServiceService.deleteAssignmentDetails(this.deleteAssignmentIntermediate['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAllAssignments(0,10)
      });
  }




}
