import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApplicationConfigurationService} from "../../services/configservice/application-configuration.service";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
declare var $: any;

@Component({
  selector: 'app-createassigmentpop-quiz',
  templateUrl: './createassigmentpop-quiz.component.html',
  styleUrls: ['./createassigmentpop-quiz.component.css']
})
export class CreateassigmentpopQuizComponent implements OnInit {


  public assignmentObj:any={
    inClassAssignment:false,
    takeHomeAssignment:false
  }
  public assignmentPopUpDetails:any={
    subjectList:[],
    gradesList:[],
    assignmentTypeList:[],
    assignmentWeightList:[],

  }

  constructor(public router:Router,private configurationServiceService:ApplicationConfigurationService,
              private assignmentServiceService:AssignmentServiceService) { }


  fnToggale(optionName){
    if(optionName==="inClass"){
      this.assignmentObj.inClassAssignment=true
    }
    else{
        this.assignmentObj.takeHomeAssignment=true
    }

    $(".checbBtn").click(function(){
      $(".checbBtn").removeClass("active")
      $(this).addClass("active")
    })
  }

 


  closePopUP(){
    $(".btn-create").hide()
  }


  getAllSubjects() {
    this.configurationServiceService.getAllSubjects()
      .subscribe((subjectList: any) => {
        console.log("subjectListsubjectListsubjectList???")
        console.log(subjectList)
        this.assignmentPopUpDetails.subjectList=subjectList;

      })
  }


  getAllGrades(){
    this.configurationServiceService.getAllGradingType()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPopUpDetails.gradesList=data;
      });
  }

  getAllAssignmentTypes(){
    this.configurationServiceService.getAllAssignmentType()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPopUpDetails.assignmentTypeList=data;
      });
  }

  getAllAssignmentWeights(){
    this.configurationServiceService.getAllAssignmentWeight()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPopUpDetails.assignmentWeightList=data;
      });
  }

  getAllClassDetails(){
    this.configurationServiceService.getAllClassDetails()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPopUpDetails.classDetails= data;
      });
  }


  getAllSemesterDetails(){
    this.configurationServiceService.getAllSemester()
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentPopUpDetails.semesterDetails= data;
      });
  }
  ngOnInit() {
    this.getAllSubjects();
    this.getAllGrades();
    this.getAllClassDetails();
    this.getAllSemesterDetails();
    this.getAllAssignmentTypes()
    this.getAllAssignmentWeights()

  }

  navigateToCreateAssignment(){
    console.log("99999999999999assignmentObj")
    console.log(this.assignmentObj)
    this.assignmentServiceService.setAssginmentOption(this.assignmentObj)
    this.router.navigate(['createAssignment']);
  }
}
