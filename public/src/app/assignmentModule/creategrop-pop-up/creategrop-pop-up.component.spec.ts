import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreategropPopUpComponent } from './creategrop-pop-up.component';

describe('CreategropPopUpComponent', () => {
  let component: CreategropPopUpComponent;
  let fixture: ComponentFixture<CreategropPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreategropPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreategropPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
