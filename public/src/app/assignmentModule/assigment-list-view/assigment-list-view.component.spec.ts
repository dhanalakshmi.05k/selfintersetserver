import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssigmentListViewComponent } from './assigment-list-view.component';

describe('AssigmentListViewComponent', () => {
  let component: AssigmentListViewComponent;
  let fixture: ComponentFixture<AssigmentListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigmentListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigmentListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
