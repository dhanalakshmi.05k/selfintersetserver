import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
declare var $: any;
@Component({
  selector: 'app-assigment-list-view',
  templateUrl: './assigment-list-view.component.html',
  styleUrls: ['./assigment-list-view.component.css']
})
export class AssigmentListViewComponent implements OnInit {
  selectedDevice="Assignment"
  recordAvailableStatus=false
  assignmentToDelete:any={}
  deleteAssignmentIntermediate:any={}
  currentUser:any
  isListViewActive:boolean=false
  isGridViewActive:boolean=false
  localIndex:number
  assignmentList:any=[]
  constructor(private router: Router,private assignmentServiceService:AssignmentServiceService) {
    console.log("global user ???????????????????")
    console.log( JSON.parse(localStorage.getItem('currentUser')))
    this.currentUser=JSON.parse(localStorage.getItem('currentUser')).user
  }

  zoomAdded(assignmentDetails){
    console.log(assignmentDetails)
    this.localIndex=assignmentDetails.index
    $(".ListViewHolder").click(function(){
      $(".ListViewHolder").removeClass("selected")
      $(this).addClass("selected")
    })
   // this.navigateToEditAssignment(assignmentDetails)
  }

  popShowfn(){
    // $(".edit").click(function(){
       
    // }
  }

  showPopup(){
    $("#createOptionPopUp").removeClass("DN")
    $("#createOptionPopUp").addClass("DB")
  }

  ngOnInit() {
    this.assignmentServiceService.setAssginmentStatus(false)
    this.getAllAssignments(0,10)
  }

  getAllAssignments(skip,limit){
    this.assignmentServiceService.getAllAssignmentDetailsBYFacultyName(this.currentUser)
      .subscribe((data: any) => {
        for(var j=0;j<data.length;j++){
          data[j]['index']=j
        }
        console.log(data);
        this.assignmentList= data.reverse();
      });
  }
  showRecordsBasedonType(recordType){
    console.log("************clicekd called"+recordType)
    if(recordType==="all"||recordType==="Assignment"){
      this.recordAvailableStatus=true
      this.getAllAssignments(0,10);
    }
    else{
      this.assignmentList=[]
      this.recordAvailableStatus=false
    }

  }



  navigateListView(){
    this.isGridViewActive=false
    this.isListViewActive=true
    this.router.navigate(['assignmentListView']);
  }
  navigateAssignment(){
    this.router.navigate(['mainDashBoard']);
  }
  navigateDashBoardView(){
    this.isGridViewActive=true
    this.isListViewActive=false
    this.router.navigate(['mainDashBoard']);
  }


  navigateToDeleteAssignment(assignmnentDetails){
this.assignmentToDelete=assignmnentDetails
  }

  navigateToEditAssignment(assignmnentEditDetails){
    console.log(assignmnentEditDetails)
    this.router.navigate(['createAssignment',assignmnentEditDetails['_id']]);

  }

  deleteAssignmentIntermediateDetails(assign){
    this.deleteAssignmentIntermediate=assign
  }

  deleteAssignmentDetails(){
    this.assignmentServiceService.deleteAssignmentDetails(this.deleteAssignmentIntermediate['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAllAssignments(0,10)
      });
  }

}
