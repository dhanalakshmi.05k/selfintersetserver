import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {StudentDetailsService} from "../../services/usermanagement/student-details.service";
import {AssignmentGroupService} from "../../services/assignmentService/assignment-group.service";
import {AssignmentServiceService} from "../../services/assignmentService/assignment-service.service";
import {AssignmentStudentService} from "../../services/assignmentService/assignment-student.service";
import {ConfigurationServiceService} from "../../services/configuration/configuration-service.service";
declare var $: any;
import * as html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf';

@Component({
    selector: 'app-assignmentpublish',
    templateUrl: './assignmentpublish.component.html',
    styleUrls: ['./assignmentpublish.component.css']
})
export class AssignmentpublishComponent implements OnInit {
    public isNotifyOffline=false
    public isGroupPublish=false
    private studentList:any=[];
    private sendStudentEmailList:any=[]
    private showAssignmentStatusDetails:any=[]
    private GroupDetailsList:any=[]
    public assignmentParameterDetails:any={}
    public groupDetails:any
    public GroupIdList:any
    assignmentDeatilsParamsId:any;
    questionDetailsList:any;
    constructor(public router:Router,private assignmentStudentService:AssignmentStudentService
        ,private studentDetailsService:StudentDetailsService
        ,private assignmentGroupService:AssignmentGroupService, private activeRoute: ActivatedRoute,
                private assignmentService:AssignmentServiceService,private configurationServiceService:ConfigurationServiceService) {

    }

    ngOnInit() {
        const queryParams = this.activeRoute.snapshot.queryParams
        const routeParams = this.activeRoute.snapshot.params;
        this.assignmentDeatilsParamsId=routeParams.id
        if(routeParams.id2=== "group"){
            this.isGroupPublish=true
            this.assignmentDeatilsParamsId=routeParams.id
            this.getAssignmentDetailsByParam(routeParams.id)

        }

        else if(routeParams.id2=== "perStudent"){
            this.isGroupPublish=false
            this.assignmentDeatilsParamsId=routeParams.id
            this.getAssignmentDetailsByParam(routeParams.id)

        }

    }
    getAssignmentDetailsByParam(assignmentId){

        this.assignmentService.getAssignmentDetails(assignmentId)
            .subscribe((data: any) => {

                console.log("data?????????????????????????")
                console.log(data[0])
                this.assignmentParameterDetails=data[0]
                this.showAssignmentStatusDetails=data[0]
                this.assignmentService.setAssginmentStudentDetails(data[0])


            });
    }


    generatePDF(){
        // parentdiv is the html element which has to be converted to PDF
        html2canvas(document.querySelector("#printPreview")).then(canvas => {
            /*  'p', 'pt', */
            /*var pdf = new jsPDF('p', 'pt','2000');

            var imgData  = canvas.toDataURL("image/jpeg", 1.0);
            pdf.addImage(imgData,0,0,canvas.width, canvas.height);
            pdf.save('monitoringReports.pdf');
      */

            var imgData  = canvas.toDataURL("image/jpeg", 1.0);

            var doc = new jsPDF('p', 'mm', 'a3');  // optional parameters
            doc.addImage(imgData, 'PNG', 1, 2);
            doc.save("monitoringReports.pdf");
        });

    }


    getAssignmentQuestionDetails(){
        let that=this
        console.log("?????????data?????========that.assignmentDeatilsParamsId================???")
        console.log(that.assignmentDeatilsParamsId)
        that.assignmentService.getAssignmentQuesDetails(that.assignmentDeatilsParamsId)
            .subscribe((data: any) => {

                console.log("?????????data?????========================???")
                console.log(data[0].AssignmentQuestion)
                that.questionDetailsList=data[0].AssignmentQuestion
                console.log(that.questionDetailsList)

            });
    }

    addPublishclass(){
        $("#publishStatus").addClass("on")
    }

    groupAssignmentDetails(grouplist){
        for(let m=0;m<grouplist;m++){
            this.assignmentGroupService.getAssignmentGroupId(grouplist[m])
                .subscribe((data: any) => {
                    console.log(data)
                    this.GroupDetailsList.push(data[0])
                })
        }
        console.log("this.GroupDetailsList>>>>>>")
        console.log(this.GroupDetailsList)
        this. formEmailList();
    }


    formEmailList(){
        for(var l=0;l<this.GroupDetailsList.length;l++){
            console.log("this.formEmailList>>>>>formEmailListformEmailList>")
            console.log(this.GroupDetailsList[l])
        }
    }


    getAllAssignmentGroupDetailsByAssignmentId(assignmentId){
        let publishStudentList:any=[]
        this.assignmentGroupService.getAssignmentGroupDetailsByAssignmentId(assignmentId)
            .subscribe((data: any) => {
                console.log("getting groupmdetails------questionsService-------");
                console.log(data);
                for(let l=0;l<data.length;l++){
                    for(let m=0;m<data[l].studentDetails;m++){
                        publishStudentList.push(data[l].studentDetails[m].studentName)
                    }
                }
                this.sendEmailStudentList(publishStudentList)
            });
    }

    sendEmailStudentList(studentPublishList){

        console.log("Before sending email-------------");
        console.log(studentPublishList);

        /*    this.studentDetailsService.notifyAssignmentDetailsToStudents(studentPublishList)
              .subscribe(response => {

              });*/
        /*this.router.navigate(['mainDashBoard'])*/
    }
    status: boolean = false;
    fnTOggle(){
        this.status = !this.status;
    }



    getAllAssignmentGroupDetails(){
        let assignmentDetails=this.showAssignmentStatusDetails;

        let publishStudentList:any=[]
        let assGroupDetails:any=[]
        for(let r=0;r<assignmentDetails['assginmentGroupId'].length;r++){
            this.assignmentGroupService.getAssignmentGroupId(assignmentDetails['assginmentGroupId'][r])
                .subscribe((data: any) => {
                    for(let m=0;m<data[0].studentDetails.length;m++){
                        publishStudentList.push(data[0].studentDetails[m].studentEmail)
                    }
                    this.studentDetailsService.notifyAssignmentDetailsToStudents(publishStudentList)
                        .subscribe(response => {
                            console.log("email status--------")
                            console.log(response)
                        });
                })

            console.log("email status--------")
            console.log(assignmentDetails['assginmentGroupId'].length)
            console.log(r)
            /*  if(r==assignmentDetails['assginmentGroupId'].length-1){

              }*/
        }
        this.router.navigate(['mainDashBoard']);
        this.assignmentService.setAssginmentStatus(true)

    }
    formStudentLsit(assGroupDetails){
        console.log("formStudentLsit----questionsService-------");
        console.log(assGroupDetails);
    }
    /* publishStudentAssign(){
       console.log("kdklsdjksjdf---studentstudent-----")
       console.log(this.groupDetails.studentDetails)
       /!*    let obj={}
           obj['studentEmail']=student['studentDetails']['studentEmail']
           obj['studentId']=student['_id']*!/
       this.studentList.push(this.groupDetails.studentDetails['studentEmail']);
       console.log("this.selectedStudentsForAssignment.8098rrrrrrrrrrrrrrrrstudentDetails44444444444rrrrr")
       console.log(this.studentList)
       this.publishEmailToStudents()
     }

     publishEmailToStudents(){
       console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
       console.log(this.studentList)
    /!*   this.studentDetailsService.notifyAssignmentDetailsToStudents(this.studentList)
         .subscribe(response => {
           console.log("kdklsdjksjdf--------")
           console.log(response)
         });*!/
    this.assignmentService.setAssginmentStatus()
       this.router.navigate(['mainDashBoard']);

     }*/
    notififyOffline(){
        this.isNotifyOffline=true

    }
    publishToAllstudentsPerAssignment(){
        let publishIndStudentList:any=[]
        this.assignmentStudentService.getStudentsByAssignmentID(this.assignmentDeatilsParamsId)
            .subscribe((data: any) => {

                for(let m=0;m<data.length;m++){
                    console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
                    console.log(data[m]['studentDetails'])
                    publishIndStudentList.push(data[m]['studentDetails']['studentEmail'])
                    // for(let j=0;j<data[m]['studentDetails'].length;j++){
                    //
                    //
                    // }
                }
                console.log("this.selectedStudentsForA----------------------4444444444rrrrr")
                console.log(publishIndStudentList)
                this.studentDetailsService.notifyAssignmentDetailsToStudents(publishIndStudentList)
                    .subscribe(response => {
                        console.log("email status--------")
                        console.log(response)
                    });
                this.router.navigate(['mainDashBoard']);
                this.assignmentService.setAssginmentStatus(true)
            });
    }


    moveToCreateGroup(){
        this.router.navigate(['createGroup']);
    }

    navigateToListAssignmentView(){
        console.log(this.assignmentDeatilsParamsId)
        this.router.navigate(['showAssignmentQuestions',this.assignmentDeatilsParamsId]);

    }

    getAllGrades(){
        /*   this.configurationServiceService.getAllGradingTools()
             .subscribe((data: any) => {
               console.log(data);
               this.assignmentPublishDetails.gradesList=data;
             });*/
    }

}
