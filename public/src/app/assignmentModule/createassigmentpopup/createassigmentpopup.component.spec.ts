import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateassigmentpopupComponent } from './createassigmentpopup.component';

describe('CreateassigmentpopupComponent', () => {
  let component: CreateassigmentpopupComponent;
  let fixture: ComponentFixture<CreateassigmentpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateassigmentpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateassigmentpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
