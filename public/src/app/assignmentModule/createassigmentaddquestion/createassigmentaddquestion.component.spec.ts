import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateassigmentaddquestionComponent } from './createassigmentaddquestion.component';

describe('CreateassigmentaddquestionComponent', () => {
  let component: CreateassigmentaddquestionComponent;
  let fixture: ComponentFixture<CreateassigmentaddquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateassigmentaddquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateassigmentaddquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
