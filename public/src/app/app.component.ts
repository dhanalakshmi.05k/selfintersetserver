import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private router: Router) { }



fnWidHgt(){
  var bh = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
  console.log("h:" +bh)
  var appheight = document.getElementById("appWrap")
  appheight.style.minHeight = bh + "px";
  appheight.style.height = "100%"
}

ngOnInit() {
  this.fnWidHgt();
}



  title = 'ProjectADA';
}
