import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentSkillDetailComponent } from './assignment-skill-detail.component';

describe('AssignmentSkillDetailComponent', () => {
  let component: AssignmentSkillDetailComponent;
  let fixture: ComponentFixture<AssignmentSkillDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentSkillDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentSkillDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
