import { Component, OnInit } from '@angular/core';
import {AssignmentSkillDetailService} from "../../services/applicationService/assignment-skill-detail.service";
declare var $: any;

@Component({
  selector: 'app-assignment-skill-detail',
  templateUrl: './assignment-skill-detail.component.html',
  styleUrls: ['./assignment-skill-detail.component.css']
})
export class AssignmentSkillDetailComponent implements OnInit {

assignmentSkillDetailObj:any={}
assignmentSkillDetailList:any=[]
isEdit:boolean=false
deleteskilldetailObj:any={}

constructor(public assignmentSkillDetailService:AssignmentSkillDetailService) { }

  ngOnInit() {
    this.getAssignmentSkillDetails(0,10);

  }
  navigateToCreateAssignmentSkillDetailPopup(){

    $("#createAssignmentSkillDetail").removeClass("DB")
    $("#createAssignmentSkillDetail").addClass("DN")
  }




  fnDeleteHidePop(){
    $("#deleteAssignmentSkillDetail").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentSkillDetail(){
    this.assignmentSkillDetailService.saveAssignmentSkillDetails(this.assignmentSkillDetailObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
        this.assignmentSkillDetailList.unshift(data);

      });
  }

  getAssignmentSkillDetails(skip ,limit){
    this.assignmentSkillDetailService.getAllAssignmentSkillDetail(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentSkillDetailList=data
      });
  }

  editIntermediatePopUp(skilldetailObj){
  this.EditOpenPopup()
    this.isEdit=true
    this.assignmentSkillDetailObj=skilldetailObj

  }

  updateAssignmentSkillDetail(){
    this.assignmentSkillDetailService.updateAssignmentSkillDetailById(this.assignmentSkillDetailObj['_id'],this.assignmentSkillDetailObj)
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentSkillDetails(0,10);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(skilldetailObj){
    this.deleteskilldetailObj=skilldetailObj
    this.openDeletePopup()
  }

  deleteAssignmentSkillDetail(skilldetailObj){
    this.assignmentSkillDetailService.deleteAssignmentSkillDetailByMongodbId(skilldetailObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentSkillDetails(0,10);
        this.fnDeleteHidePop()
      });
  }

  EditOpenPopup(){

    $("#createAssignmentSkillDetail").removeClass("DN")
    $("#createAssignmentSkillDetail").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentSkillDetailObj={}
    $("#createAssignmentSkillDetail").removeClass("DN")
    $("#createAssignmentSkillDetail").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentSkillDetail").addClass("DN")
    $("#createAssignmentSkillDetail").removeClass("DB")
    this.getAssignmentSkillDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentSkillDetail").removeClass("DN")
    $("#deleteAssignmentSkillDetail").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentSkillDetail").addClass("DN")
    $("#deleteAssignmentSkillDetail").removeClass("DB")
  }



}
