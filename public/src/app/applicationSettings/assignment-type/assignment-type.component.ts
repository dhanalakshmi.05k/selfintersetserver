import { Component, OnInit } from '@angular/core';
import {AssignmentTypeService} from "../../services/applicationService/assignment-type.service";
declare var $: any;

@Component({
  selector: 'app-assignment-type',
  templateUrl: './assignment-type.component.html',
  styleUrls: ['./assignment-type.component.css']
})
export class AssignmentTypeComponent implements OnInit {

assignmentTypeObj:any={}
assignmentTypeList:any=[]
isEdit:boolean=false
deletetypeObj:any={}

constructor(public assignmentTypeService:AssignmentTypeService) { }

  ngOnInit() {
    this.getAssignmentTypeDetails(0,10);

  }
  navigateToCreateAssignmentTypePopup(){
    this.isEdit=false
    $("#createAssignmentType").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentType").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentType").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentType(){

    this.assignmentTypeService.saveAssignmentTypeDetails(this.assignmentTypeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentTypeList.unshift(data);
        this.hidePopup()
      });
  }

  getAssignmentTypeDetails(skip ,limit){
    this.assignmentTypeService.getAllAssignmentType(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentTypeList=data.reverse()
      });
  }

  editIntermediatePopUp(typeObj){
    this.isEdit=true
    this.assignmentTypeObj=typeObj
   this.EditOpenPopup()
  }

  updateAssignmentType(){
    this.assignmentTypeService.updateAssignmentTypeById(this.assignmentTypeObj['_id'],this.assignmentTypeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(typeObj){
    this.deletetypeObj=typeObj
    this.openDeletePopup()
  }

  deleteAssignmentType(typeObj){
    this.assignmentTypeService.deleteAssignmentTypeByMongodbId(typeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentTypeDetails(0,10);
        this.closeDeletePopup()
      });
  }

  EditOpenPopup(){

    $("#createAssignmentType").removeClass("DN")
    $("#createAssignmentType").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentTypeObj={}
    $("#createAssignmentType").removeClass("DN")
    $("#createAssignmentType").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentType").addClass("DN")
    $("#createAssignmentType").removeClass("DB")
    this.getAssignmentTypeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentType").removeClass("DN")
    $("#deleteAssignmentType").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentType").addClass("DN")
    $("#deleteAssignmentType").removeClass("DB")
  }



}
