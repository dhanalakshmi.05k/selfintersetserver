import { Component, OnInit } from '@angular/core';
import {YearDetailsService} from "../../services/applicationService/year-details.service";
declare var $: any;
@Component({
  selector: 'app-year-details',
  templateUrl: './year-details.component.html',
  styleUrls: ['./year-details.component.css']
})
export class YearDetailsComponent implements OnInit {

  assignmentBadgeObj:any={}
  assignmentYearList:any=[]
  isEdit:boolean=false
  deletebadgeObj:any={}

  constructor(public yearDetailsService:YearDetailsService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateAssignmentYearPopup(){
    this.isEdit=false
    $("#createAssignmentYear").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentYear").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentYear").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentYear(){

    this.yearDetailsService.saveYearDetailsDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentYearList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.yearDetailsService.getAllYear(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentYearList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentBadgeObj=badgeObj
     this.EditOpenPopup()
  }

  updateAssignmentYear(){
    this.yearDetailsService.updateYearById(this.assignmentBadgeObj['_id'],this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.yearDetailsService.deleteYearByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createAssignmentYear").removeClass("DN")
    $("#createAssignmentYear").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createAssignmentYear").removeClass("DN")
    $("#createAssignmentYear").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentYear").addClass("DN")
    $("#createAssignmentYear").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentYear").removeClass("DN")
    $("#deleteAssignmentYear").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentYear").addClass("DN")
    $("#deleteAssignmentYear").removeClass("DB")
  }

}
