import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentReasonComponent } from './assignment-reason.component';

describe('AssignmentReasonComponent', () => {
  let component: AssignmentReasonComponent;
  let fixture: ComponentFixture<AssignmentReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
