import { Component, OnInit } from '@angular/core';
import {AssignmentReasonService} from "../../services/applicationService/assignment-reason.service";
declare var $: any;

@Component({
  selector: 'app-assignment-reason',
  templateUrl: './assignment-reason.component.html',
  styleUrls: ['./assignment-reason.component.css']
})
export class AssignmentReasonComponent implements OnInit {

assignmentReasonObj:any={}
assignmentReasonList:any=[]
isEdit:boolean=false
deletereasonObj:any={}

constructor(public assignmentReasonService:AssignmentReasonService) { }

  ngOnInit() {
    this.getAssignmentReasonDetails(0,10);

  }
  navigateToCreateAssignmentReasonPopup(){
    this.isEdit=false
    $("#createAssignmentReason").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentReason").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentReason").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentReason(){

    this.assignmentReasonService.saveAssignmentReasonDetails(this.assignmentReasonObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentReasonList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentReasonDetails(skip ,limit){
    this.assignmentReasonService.getAllAssignmentReason(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentReasonList=data.reverse()
      });
  }

  editIntermediatePopUp(reasonObj){
    this.isEdit=true
    this.assignmentReasonObj=reasonObj
    this.EditOpenPopup()
  }

  updateAssignmentReason(){
    this.assignmentReasonService.updateAssignmentReasonById(this.assignmentReasonObj['_id'],this.assignmentReasonObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()

      });
  }

  deleteIntermediatePopUp(reasonObj){
    this.deletereasonObj=reasonObj
    this.openDeletePopup()
  }

  deleteAssignmentReason(reasonObj){
    this.assignmentReasonService.deleteAssignmentReasonByMongodbId(reasonObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentReasonDetails(0,10);
        this.closeDeletePopup()
      });
  }

 EditOpenPopup(){

    $("#createAssignmentReason").removeClass("DN")
    $("#createAssignmentReason").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentReasonObj={}
    $("#createAssignmentReason").removeClass("DN")
    $("#createAssignmentReason").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentReason").addClass("DN")
    $("#createAssignmentReason").removeClass("DB")
    this.getAssignmentReasonDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentReason").removeClass("DN")
    $("#deleteAssignmentReason").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentReason").addClass("DN")
    $("#deleteAssignmentReason").removeClass("DB")
  }



}
