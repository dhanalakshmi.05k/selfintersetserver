import { Component, OnInit } from '@angular/core';
import {UserManagementService} from "../../services/usermanagement/user-management.service";
declare var $: any;

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {


assignmentBadgeObj:any={}
assignmentUserList:any=[]
isEdit:boolean=false
deletebadgeObj:any={}

constructor(public userManagementService:UserManagementService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateAssignmentUserPopup(){
    this.isEdit=false
    $("#createAssignmentUser").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentUser").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentUser").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentUser(){

    this.userManagementService.saveUserDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentUserList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.userManagementService.getAllUser(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentUserList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentBadgeObj=badgeObj
     this.EditOpenPopup()
  }

  updateAssignmentUser(){
    this.userManagementService.updateUserById(this.assignmentBadgeObj['_id'],this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.userManagementService.deleteUserByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createAssignmentUser").removeClass("DN")
    $("#createAssignmentUser").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createAssignmentUser").removeClass("DN")
    $("#createAssignmentUser").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentUser").addClass("DN")
    $("#createAssignmentUser").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentUser").removeClass("DN")
    $("#deleteAssignmentUser").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentUser").addClass("DN")
    $("#deleteAssignmentUser").removeClass("DB")
  }

}
