import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentBadgeComponent } from './assignment-badge.component';

describe('AssignmentBadgeComponent', () => {
  let component: AssignmentBadgeComponent;
  let fixture: ComponentFixture<AssignmentBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
