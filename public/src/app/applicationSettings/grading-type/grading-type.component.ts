import { Component, OnInit } from '@angular/core';
import {GradingTypeService} from "../../services/applicationService/grading-type.service";
declare var $: any;

@Component({
  selector: 'app-grading-type',
  templateUrl: './grading-type.component.html',
  styleUrls: ['./grading-type.component.css']
})
export class GradingTypeComponent implements OnInit {

gradingTypeObj:any={}
gradingTypeList:any=[]
isEdit:boolean=false
  deletetypeObj:any={}

constructor(public gradingTypeService:GradingTypeService) { }

  ngOnInit() {
    this.getGradingTypeDetails(0,10);

  }
  navigateToCreateGradingTypePopup(){
    this.isEdit=false
    $("#createGradingType").addClass("DB")
  }


  fnHidePop(){
    $("#createGradingType").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteGradingType").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveGradingType(){

    this.gradingTypeService.saveGradingTypeDetails(this.gradingTypeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.gradingTypeList.unshift(data);
        this.hidePopup()
      });
  }

  getGradingTypeDetails(skip ,limit){
    this.gradingTypeService.getAllGradingTypes(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.gradingTypeList=data.reverse()
      });
  }

  editIntermediatePopUp(typeObj){
    this.isEdit=true
    this.gradingTypeObj=typeObj
   this.EditOpenPopup()
  }

  updateGradingType(){
    this.gradingTypeService.updateGradingTypeById(this.gradingTypeObj['_id'],this.gradingTypeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(typeObj){
    this.deletetypeObj=typeObj
    this.openDeletePopup()
  }

  deleteGradingType(typeObj){
    this.gradingTypeService.deleteGradingTypeByMongodbId(typeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getGradingTypeDetails(0,10);
        this.closeDeletePopup()
      });
  }

  EditOpenPopup(){

    $("#createGradingType").removeClass("DN")
    $("#createGradingType").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.gradingTypeObj={}
    $("#createGradingType").removeClass("DN")
    $("#createGradingType").addClass("DB")
  }
  hidePopup(){
    $("#createGradingType").addClass("DN")
    $("#createGradingType").removeClass("DB")
    this.getGradingTypeDetails(0,10);

  }
  openDeletePopup() {

    $("#deleteGradingType").removeClass("DN")
    $("#deleteGradingType").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteGradingType").addClass("DN")
    $("#deleteGradingType").removeClass("DB")
  }



}
