import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx'
import {StudentDetailsService} from "../../services/usermanagement/student-details.service";

@Component({
  selector: 'app-settings-dashboard',
  templateUrl: './settings-dashboard.component.html',
  styleUrls: ['./settings-dashboard.component.css']
})
export class SettingsDashboardComponent implements OnInit {
  studentList:any=[]
  facultyList:any=[]
  listIntermediateData:any=[]
  constructor(public router:Router,private studentDetailsService:StudentDetailsService) { }

  ngOnInit() {
  }
  navigateToAssignmentBadge(){
    this.router.navigate(['assignmentBadge']);
  }
  navigateToAssignmentCategory(){
    this.router.navigate(['assignmentCategory']);
  }
  navigateToAssignmentLOP(){
    this.router.navigate(['assignmentLevelOfProficiency']);
  }
  navigateToAssignmentReason(){
    this.router.navigate(['assignmentReason']);
  }
  navigateToAssignmentSkill(){
    this.router.navigate(['assignmentSkillDetail']);
  }
  navigateToAssignmentSplitBoard(){
    this.router.navigate(['assignmentSplitBoard']);
  }
  navigateToAssignmentType(){
    this.router.navigate(['assignmentType']);
  }
  navigateToAssignmentWeight(){
    this.router.navigate(['assignmentWeight']);
  }

  navigateToAssignmentSemester(){
    this.router.navigate(['semesterDetails']);
  }

  navigateToAssignmentYear(){
    this.router.navigate(['yearDetails']);
  }
  navigateToGradingTool(){
    this.router.navigate(['gradingTool']);
  }
  navigateToGradingType(){
    this.router.navigate(['gradingType']);
  }
   navigateToAssignmentFocus(){
    this.router.navigate(['assignmentFocus']);
  }
   navigateToHabitsOfLearner(){
    this.router.navigate(['habitsOfLearner']);
  }
   navigateToTeacherBadge(){
    this.router.navigate(['TeacherBadge']);
  }
   navigateToSubjectDetails(){
    this.router.navigate(['subjectDetails']);
  }
 navigateToClassDetails(){
    this.router.navigate(['classDetails']);
  }
navigateToUserManagement(){
    this.router.navigate(['userManagement']);
  }

  uploadImage(event){
  let that=this;
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
      var fileData = reader.result;
      var wb = XLSX.read(fileData, {type : 'binary'});
      var tableObjs;
      wb.SheetNames.forEach(function(sheetName){
        var rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
        var tableObjs=JSON.stringify(rowObj);
        console.log(tableObjs)
        if (tableObjs == undefined || tableObjs== null){
          return;
        }
        else{
          console.log(JSON.stringify(rowObj))
          that.studentList=JSON.stringify(rowObj)
          that.saveStudentDetails(that.studentList)
        }
      })
    };

    reader.readAsBinaryString(input.files[0])


  }

  saveStudentDetails(listData){
    console.log("intermediate")
    console.log("saveCC1DetaisToDB-----------listData-------")

    var arrayListData = JSON.parse("[" + listData + "]");
    for(let p=0;p<arrayListData[0].length;p++){
      console.log("saveCC1DetaisToDB------------------")
      console.log(arrayListData[0][p])

      var obj={}
      obj['studentName']=arrayListData[0][p]['studentName']
      obj['studentEmail']=arrayListData[0][p]['studentEmail']
      obj['studentAttendance']=arrayListData[0][p]['studentAttendance']
      obj['studentPeformance']=arrayListData[0][p]['studentPeformance']
      this.studentDetailsService.saveStudentDetails(obj)
        .subscribe((saveDetails: any) =>{

        })
    }
    alert("student Details updated successfully")
  }

  uploadFacultyImage(event){
    let that=this;
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
      var fileData = reader.result;
      var wb = XLSX.read(fileData, {type : 'binary'});
      var tableObjs;
      wb.SheetNames.forEach(function(sheetName){
        var rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
        var tableObjs=JSON.stringify(rowObj);
        console.log(tableObjs)
        if (tableObjs == undefined || tableObjs== null){
          return;
        }
        else{
          console.log(JSON.stringify(rowObj))
          that.facultyList=JSON.stringify(rowObj)
          that.saveFacultyListDetails(that.facultyList)
        }
      })
    };

    reader.readAsBinaryString(input.files[0])
  }

  saveFacultyListDetails(listData){
    console.log("intermediate")
    console.log("saveCC1DetaisToDB-----------listData-------")

    var arrayListData = JSON.parse("[" + listData + "]");
    for(let p=0;p<arrayListData[0].length;p++){
      console.log("saveCC1DetaisToDB------------------")
      console.log(arrayListData[0][p])
      var obj={}
      obj['studentName']=arrayListData[0][p]['studentName']
      obj['studentEmail']=arrayListData[0][p]['studentEmail']
      obj['studentAttendance']=arrayListData[0][p]['studentAttendance']
      obj['studentPeformance']=arrayListData[0][p]['studentPeformance']
      this.studentDetailsService.saveStudentDetails(obj)
        .subscribe((saveDetails: any) =>{

        })
    }
    alert("student Details updated successfully")
  }
}
