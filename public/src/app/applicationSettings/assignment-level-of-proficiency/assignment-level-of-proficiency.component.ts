import { Component, OnInit } from '@angular/core';
import {AssignmentLevelOfProficiencyService} from "../../services/applicationService/assignment-level-of-proficiency.service";
declare var $: any;

@Component({
  selector: 'app-assignment-level-of-proficiency',
  templateUrl: './assignment-level-of-proficiency.component.html',
  styleUrls: ['./assignment-level-of-proficiency.component.css']
})
export class AssignmentLevelOfProficiencyComponent implements OnInit {

assignmentLevelOfProficiencyObj:any={}
assignmentLevelOfProficiencyList:any=[]
isEdit:boolean=false
deletelevelofproficiencyObj:any={}

constructor(public assignmentLevelOfProficiencyService:AssignmentLevelOfProficiencyService) { }

  ngOnInit() {
    this.getAssignmentLevelOfProficiencyDetails(0,10);

  }
  navigateToCreateAssignmentLevelOfProficiencyPopup(){
    this.isEdit=false
    $("#createAssignmentLevelOfProficiency").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentLevelOfProficiency").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentLevelOfProficiency").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentLevelOfProficiency(){




    this.assignmentLevelOfProficiencyService.saveAssignmentLevelOfProficiencyDetails(this.assignmentLevelOfProficiencyObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentLevelOfProficiencyList.unshift(data)
         this.hidePopup()
      });
  }

  getAssignmentLevelOfProficiencyDetails(skip ,limit){
    this.assignmentLevelOfProficiencyService.getAllAssignmentLevelOfProficiency(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentLevelOfProficiencyList=data.reverse()
      });
  }

  editIntermediatePopUp(LevelOfProficiencyObj){
    this.isEdit=true
    this.assignmentLevelOfProficiencyObj=LevelOfProficiencyObj
    this.EditOpenPopup()
  }

  updateAssignmentLevelOfProficiency(){
    this.assignmentLevelOfProficiencyService.updateAssignmentLevelOfProficiencyById(this.assignmentLevelOfProficiencyObj['_id'],this.assignmentLevelOfProficiencyObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(levelofproficiencyObj){
    this.deletelevelofproficiencyObj=levelofproficiencyObj
    this.openDeletePopup()
  }

  deleteAssignmentLevelOfProficiency(levelofproficiencyObj){
    this.assignmentLevelOfProficiencyService.deleteAssignmentLevelOfProficiencyByMongodbId(levelofproficiencyObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentLevelOfProficiencyDetails(0,10);
        this.closeDeletePopup()
      });
  }

EditOpenPopup(){

    $("#createAssignmentLevelOfProficiency").removeClass("DN")
    $("#createAssignmentLevelOfProficiency").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentLevelOfProficiencyObj={}
    $("#createAssignmentLevelOfProficiency").removeClass("DN")
    $("#createAssignmentLevelOfProficiency").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentLevelOfProficiency").addClass("DN")
    $("#createAssignmentLevelOfProficiency").removeClass("DB")
    this.getAssignmentLevelOfProficiencyDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentLevelOfProficiency").removeClass("DN")
    $("#deleteAssignmentLevelOfProficiency").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentLevelOfProficiency").addClass("DN")
    $("#deleteAssignmentLevelOfProficiency").removeClass("DB")
  }


}
