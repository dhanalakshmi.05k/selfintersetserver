import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentLevelOfProficiencyComponent } from './assignment-level-of-proficiency.component';

describe('AssignmentLevelOfProficiencyComponent', () => {
  let component: AssignmentLevelOfProficiencyComponent;
  let fixture: ComponentFixture<AssignmentLevelOfProficiencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentLevelOfProficiencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentLevelOfProficiencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
