import { Component, OnInit } from '@angular/core';
import {ClassDetailsService} from "../../services/applicationService/class-details.service";
declare var $: any;
@Component({
  selector: 'app-class-details',
  templateUrl: './class-details.component.html',
  styleUrls: ['./class-details.component.css']
})
export class ClassDetailsComponent implements OnInit {

assignmentBadgeObj:any={}
classDetailsList:any=[]
isEdit:boolean=false
deletebadgeObj:any={}

constructor(public classDetailsService:ClassDetailsService) { }

  ngOnInit() {
    this.getAssignmentBadgeDetails(0,10);

  }
  navigateToCreateClassDetailsPopup(){
    this.isEdit=false
    $("#createClassDetails").addClass("DB")
  }


  fnHidePop(){
    $("#createClassDetails").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteClassDetails").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveClassDetails(){

    this.classDetailsService.saveClassDetails(this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.classDetailsList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentBadgeDetails(skip ,limit){
    this.classDetailsService.getAllClassDetails(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.classDetailsList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.assignmentBadgeObj=badgeObj
     this.EditOpenPopup()
  }

  updateClassDetails(){
    this.classDetailsService.updateClassDetailsById(this.assignmentBadgeObj['_id'],this.assignmentBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteAssignmentBadge(badgeObj){
    this.classDetailsService.deleteClassDetailsByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createClassDetails").removeClass("DN")
    $("#createClassDetails").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentBadgeObj={}
    $("#createClassDetails").removeClass("DN")
    $("#createClassDetails").addClass("DB")
  }
  hidePopup(){
    $("#createClassDetails").addClass("DN")
    $("#createClassDetails").removeClass("DB")
    this.getAssignmentBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteClassDetails").removeClass("DN")
    $("#deleteClassDetails").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteClassDetails").addClass("DN")
    $("#deleteClassDetails").removeClass("DB")
  }

}
