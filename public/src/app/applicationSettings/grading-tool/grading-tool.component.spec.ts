import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingTypeComponent } from './grading-type.component';

describe('GradingTypeComponent', () => {
  let component: GradingTypeComponent;
  let fixture: ComponentFixture<GradingTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradingTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
