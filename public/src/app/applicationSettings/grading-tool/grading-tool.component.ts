import { Component, OnInit } from '@angular/core';
import {GradingToolService} from "../../services/applicationService/grading-tool.service";
declare var $: any;

@Component({
  selector: 'app-grading-tool',
  templateUrl: './grading-tool.component.html',
  styleUrls: ['./grading-tool.component.css']
})
export class GradingToolComponent implements OnInit {

gradingToolObj:any={}
gradingToolList:any=[]
isEdit:boolean=false
deletebadgeObj:any={}

constructor(public GradingToolService:GradingToolService) {
  this.isEdit=false
}

  ngOnInit() {
    this.getGradingToolDetails(0,10);

  }
  navigateToCreateGradingToolPopup(){
    this.isEdit=false
    $("#createGradingTool").addClass("DB")
  }


  fnHidePop(){
    $("#createGradingTool").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteGradingTool").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveGradingTool(){

    this.GradingToolService.saveGradingToolDetails(this.gradingToolObj)
      .subscribe((data: any) => {
        console.log(data);
        this.gradingToolList.unshift(data)
        this.hidePopup()
      });
  }

  getGradingToolDetails(skip ,limit){
    this.GradingToolService.getAllGradingTools(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.gradingToolList=data.reverse()
      });
  }

  editIntermediatePopUp(badgeObj){
    this.isEdit=true
    this.gradingToolObj=badgeObj
     this.EditOpenPopup()
  }

  updateGradingTool(){
    this.GradingToolService.updateGradingToolById(this.gradingToolObj['_id'],this.gradingToolObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(badgeObj){
    this.deletebadgeObj=badgeObj
    this.openDeletePopup()
  }

  deleteGradingTool(badgeObj){
    this.GradingToolService.deleteGradingToolByMongodbId(badgeObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getGradingToolDetails(0,10);
        this.closeDeletePopup()
      });
  }
  EditOpenPopup(){

    $("#createGradingTool").removeClass("DN")
    $("#createGradingTool").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.gradingToolObj={}
    $("#createGradingTool").removeClass("DN")
    $("#createGradingTool").addClass("DB")
  }
  hidePopup(){
    $("#createGradingTool").addClass("DN")
    $("#createGradingTool").removeClass("DB")
    this.getGradingToolDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteGradingTool").removeClass("DN")
    $("#deleteGradingTool").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteGradingTool").addClass("DN")
    $("#deleteGradingTool").removeClass("DB")
  }

}
