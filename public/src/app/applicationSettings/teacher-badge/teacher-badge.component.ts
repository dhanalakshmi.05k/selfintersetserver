import { Component, OnInit } from '@angular/core';
import {TeacherBadgeService} from "../../services/applicationService/teacher-badge.service";
declare var $: any;

@Component({
  selector: 'app-teacher-badge',
  templateUrl: './teacher-badge.component.html',
  styleUrls: ['./teacher-badge.component.css']
})
export class TeacherBadgeComponent implements OnInit {

teacherBadgeObj:any={}
teacherBadgeList:any=[]
isEdit:boolean=false
deletebadgeObj:any={}

constructor(public teacherBadgeService:TeacherBadgeService) { }

  ngOnInit() {
    this.getTeacherBadgeDetails(0,10);

  }
  navigateToCreateTeacherBadgePopup(){
    this.isEdit=false
    $("#createTeacherBadge").addClass("DB")
  }


  fnHidePop(){
    $("#createTeacherBadge").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteTeacherBadge").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveTeacherBadge(){

    this.teacherBadgeService.saveTeacherBadgeDetails(this.teacherBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.teacherBadgeList.unshift(data)
        this.hidePopup()
      });
  }

  getTeacherBadgeDetails(skip ,limit){
    this.teacherBadgeService.getAllTeacherBadgeDetails(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.teacherBadgeList=data.reverse()
      });
  }

  editIntermediatePopUp(reasonObj){
    this.isEdit=true
    this.teacherBadgeObj=reasonObj
    this.EditOpenPopup()
  }

  updateTeacherBadge(){
    this.teacherBadgeService.updateTeacherBadgeDetailsById(this.teacherBadgeObj['_id'],this.teacherBadgeObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()

      });
  }

  deleteIntermediatePopUp(reasonObj){
    this.deletebadgeObj=reasonObj
    this.openDeletePopup()
  }

  deleteTeacherBadge(reasonObj){
    this.teacherBadgeService.deleteTeacherBadgeDetailsByMongodbId(reasonObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getTeacherBadgeDetails(0,10);
        this.closeDeletePopup()
      });
  }

 EditOpenPopup(){

    $("#createTeacherBadge").removeClass("DN")
    $("#createTeacherBadge").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.teacherBadgeObj={}
    $("#createTeacherBadge").removeClass("DN")
    $("#createTeacherBadge").addClass("DB")
  }
  hidePopup(){
    $("#createTeacherBadge").addClass("DN")
    $("#createTeacherBadge").removeClass("DB")
    this.getTeacherBadgeDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteTeacherBadge").removeClass("DN")
    $("#deleteTeacherBadge").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteTeacherBadge").addClass("DN")
    $("#deleteTeacherBadge").removeClass("DB")
  }



}
