import { Component, OnInit } from '@angular/core';
import {AssignmentWeightService} from "../../services/applicationService/assignment-weight.service";
declare var $: any;

@Component({
  selector: 'app-assignment-weight',
  templateUrl: './assignment-weight.component.html',
  styleUrls: ['./assignment-weight.component.css']
})
export class AssignmentWeightComponent implements OnInit {

assignmentWeightObj:any={}
assignmentWeightList:any=[]
isEdit:boolean=false
deleteweightObj:any={}

constructor(public assignmentWeightService:AssignmentWeightService) { }

  ngOnInit() {
    this.getAssignmentWeightDetails(0,10);

  }
  navigateToCreateAssignmentWeightPopup(){
    this.isEdit=false
    $("#createAssignmentWeight").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentWeight").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentWeight").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentWeight(){
    this.assignmentWeightService.saveAssignmentWeightDetails(this.assignmentWeightObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentWeightList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentWeightDetails(skip ,limit){
    this.assignmentWeightService.getAllAssignmentWeight(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentWeightList=data
      });
  }

  editIntermediatePopUp(weightObj){
    this.isEdit=true
    this.assignmentWeightObj=weightObj
   this.EditOpenPopup()
  }

  updateAssignmentWeight(){
    this.assignmentWeightService.updateAssignmentWeightById(this.assignmentWeightObj['_id'],this.assignmentWeightObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()
      });
  }

  deleteIntermediatePopUp(weightObj){
    this.deleteweightObj=weightObj
    this.openDeletePopup()
  }

  deleteAssignmentWeight(weightObj){
    this.assignmentWeightService.deleteAssignmentWeightByMongodbId(weightObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentWeightDetails(0,10);
        this.closeDeletePopup();
      });


}

  EditOpenPopup(){

    $("#createAssignmentWeight").removeClass("DN")
    $("#createAssignmentWeight").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentWeightObj={}
    $("#createAssignmentWeight").removeClass("DN")
    $("#createAssignmentWeight").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentWeight").addClass("DN")
    $("#createAssignmentWeight").removeClass("DB")
    this.getAssignmentWeightDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentWeight").removeClass("DN")
    $("#deleteAssignmentWeight").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentWeight").addClass("DN")
    $("#deleteAssignmentWeight").removeClass("DB")
  }



}
