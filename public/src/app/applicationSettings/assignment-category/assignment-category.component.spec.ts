import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentCategoryComponent } from './assignment-category.component';

describe('AssignmentCategoryComponent', () => {
  let component: AssignmentCategoryComponent;
  let fixture: ComponentFixture<AssignmentCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
