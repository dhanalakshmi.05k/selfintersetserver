import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentFocusComponent } from './assignment-focus.component';

describe('AssignmentFocusComponent', () => {
  let component: AssignmentFocusComponent;
  let fixture: ComponentFixture<AssignmentFocusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentFocusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentFocusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
