import { Component, OnInit } from '@angular/core';
import {AssignmentFocusService} from "../../services/applicationService/assignment-focus.service";
declare var $: any;

@Component({
  selector: 'app-assignment-focus',
  templateUrl: './assignment-focus.component.html',
  styleUrls: ['./assignment-focus.component.css']
})
export class AssignmentFocusComponent implements OnInit {

  assignmentFocusObj:any={}
assignmentFocusList:any=[]
isEdit:boolean=false
  deleteFocusObj:any={}

constructor(public assignmentFocusService:AssignmentFocusService) { }

  ngOnInit() {
    this.getAssignmentFocusDetails(0,10);

  }
  navigateToCreateAssignmentFocusPopup(){
    this.isEdit=false
    $("#createAssignmentFocus").addClass("DB")
  }


  fnHidePop(){
    $("#createAssignmentFocus").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }

  fnDeleteHidePop(){
    $("#deleteAssignmentFocus").addClass("DN")
    /*$(".popWrap").addClass("DN")*/
  }
  saveAssignmentFocus(){

    this.assignmentFocusService.saveAssignmentFocusDetails(this.assignmentFocusObj)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentFocusList.unshift(data)
        this.hidePopup()
      });
  }

  getAssignmentFocusDetails(skip ,limit){
    this.assignmentFocusService.getAllAssignmentFocusDetails(skip,limit)
      .subscribe((data: any) => {
        console.log(data);
        this.assignmentFocusList=data.reverse()
      });
  }

  editIntermediatePopUp(focusObj){
    this.isEdit=true
    this.assignmentFocusObj=focusObj
    this.EditOpenPopup()
  }

  updateAssignmentFocus(){
    this.assignmentFocusService.updateAssignmentFocusDetailsById(this.assignmentFocusObj['_id'],this.assignmentFocusObj)
      .subscribe((data: any) => {
        console.log(data);
        this.hidePopup()

      });
  }

  deleteIntermediatePopUp(focusObj){
    this.deleteFocusObj=focusObj
    this.openDeletePopup()
  }

  deleteAssignmentFocus(focusObj){
    this.assignmentFocusService.deleteAssignmentFocusDetailsByMongodbId(focusObj['_id'])
      .subscribe((data: any) => {
        console.log(data);
        this.getAssignmentFocusDetails(0,10);
        this.closeDeletePopup()
      });
  }

 EditOpenPopup(){

    $("#createAssignmentFocus").removeClass("DN")
    $("#createAssignmentFocus").addClass("DB")
  }

  openPopup(){
    this.isEdit=false
    this.assignmentFocusObj={}
    $("#createAssignmentFocus").removeClass("DN")
    $("#createAssignmentFocus").addClass("DB")
  }
  hidePopup(){
    $("#createAssignmentFocus").addClass("DN")
    $("#createAssignmentFocus").removeClass("DB")
    this.getAssignmentFocusDetails(0,10);
  }
  openDeletePopup() {

    $("#deleteAssignmentFocus").removeClass("DN")
    $("#deleteAssignmentFocus").addClass("DB")
  }

  closeDeletePopup() {

    $("#deleteAssignmentFocus").addClass("DN")
    $("#deleteAssignmentFocus").removeClass("DB")
  }



}
