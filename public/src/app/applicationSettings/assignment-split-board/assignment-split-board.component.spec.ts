import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentSplitBoardComponent } from './assignment-split-board.component';

describe('AssignmentSplitBoardComponent', () => {
  let component: AssignmentSplitBoardComponent;
  let fixture: ComponentFixture<AssignmentSplitBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentSplitBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentSplitBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
