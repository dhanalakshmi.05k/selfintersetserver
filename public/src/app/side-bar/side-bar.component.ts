import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
declare var $: any;

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(public router:Router) { }


 openNav() {
    document.getElementById("mySidenav").style.width = "200px";
  }
  
 closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  navigateAssignment(){
    this.router.navigate(['mainDashBoard']);
  }


  navigateSettings(){
    this.router.navigate(['applicationConfigSettings']);
  }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();  
  }

}
