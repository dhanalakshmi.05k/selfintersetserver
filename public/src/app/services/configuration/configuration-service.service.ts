import { Injectable } from '@angular/core';

import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConfigService} from "./config.service";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationServiceService {
  constructor(private http: HttpClient, public configService: ConfigService) { }
  getAllSubjects() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSubjectDetails');
  }

  getAllAssignmentWeight() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentWeight');
  }
  getAllAssignmentBadge() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentBadge');
  }
  getAllAssignmentCategory() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentCategory');
  }
  getAllAssignmentType() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentType');
  }
  getAllAssignmentLevelOfProficiency() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentLevelOfProficiency');
  }
  getAllAssignmentReason() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentReason');
  }
  getAllAssignmentSkillDetail() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSkillDetail');
  }
  getAllAssignmentSplitBoard() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSplitBoard');
  }
  getAllClassDetails() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allClassDetails');
  }
  getAllProficiency() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentLevelOfProficiency');
  }
  getAllGradingType() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingType');

  }
  getAllSemester() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSemister');
  }
  getAllYear() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allYear');
  }
  getAllStudent() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allStudent');
  }
  getAllQuetions() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allQuestions');
  }
  setCertificateImage(certificateData):Observable<any>{
    return this.http.post(this.configService.appConfig.appBaseUrl + 'setCertificateImage',certificateData);
  }

    getAllGradingTools() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingToolsForDropDown');
    }




}
