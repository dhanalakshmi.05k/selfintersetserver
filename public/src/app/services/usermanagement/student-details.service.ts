import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class StudentDetailsService {

  studentDataList:any=[]
  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveStudentDetails(studentDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addStudent', studentDetails);
  }

  getAllStudents() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allStudent/');
  }

  setStudentListDetails(studentList){
   this.studentDataList= studentList
  }
  getStudentListDetails(){
    return this.studentDataList

  }


  notifyAssignmentDetailsToStudents(studentList) {
    console.log("-09-039eqwqe")
    console.log(studentList)
    return this.http.post(this.configService.appConfig.appBaseUrl + 'publishEmailToStudents', studentList);
  }

}
