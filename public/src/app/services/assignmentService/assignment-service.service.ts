import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentServiceService {

  showAssignmentStatus:any={}
  assignmentOption:any;
  savedAssignment:any;
  showstatus:any;
  constructor(private http: HttpClient, public configService: ApiConfigService) { }

  saveAssignmentDetails(assignmentData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignment', assignmentData);
  }

  getAllAssignmentDetails(start,skip) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignment/'+start+'/'+ skip);
  }


  getAllAssignmentDetailsBYFacultyName(facultyName) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentByFacultyName/'+facultyName);
  }

  updateAssignmentByQuestion(assignmentId,assignmentDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'updateQuestionsIdsForAssignment/'+assignmentId, assignmentDetails);
  }


  getAssignmentDetails(assignmentId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'getAssignment/'+assignmentId );
  }
  saveAssignmentGroupDetails(assignmentGroupData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'assignmentGroup', assignmentGroupData);
  }

  getAllAssignmentGroups() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentAllGroup');
  }

  setAssginmentOption(assignmentDetails){
    this.assignmentOption= assignmentDetails;
  }

  getAssginmentOption(){
    return this.assignmentOption;

  }


  setAssginmentMongoDB(assignmentData){
    this.savedAssignment= assignmentData;
  }

  getAssginmentMongoDB(){
    return this.savedAssignment;

  }

  updateAssignmentDetails(assignmentDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'updateAssignmentDetailsByAssignmentId/'+assignmentDetails['_id'] , assignmentDetails);
  }

  deleteAssignmentDetails(assignmentId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentById/'+assignmentId);
  }

  setAssginmentStudentDetails(details){
    this.showAssignmentStatus= details;
  }


/*  getUserDetailsFromSession() {
    console.log("**projectada.in:5200ssssssssssssssssignin/details\ssssss**")
    console.log(this.configService.appConfig.appBaseUrl +"signin/details")
    return this.http.get(this.configService.appConfig.appBaseUrl +"signin/details");
  }*/

  getUserDetailsSession() {
    console.log("**projectada.in:5200ssssssssssssssssignin/details\ssssss**")
    console.log(this.configService.appConfig.appBaseUrl +"signin/details")
    return this.http.get(this.configService.appConfig.appBaseUrl +"signin/details");
  }

  getAssginmentStudentDetails(){
    return this.showAssignmentStatus;

  }

  setAssginmentStatus(status){
    this.showstatus= status;
  }

  getAssginmentStatus(){
    return this.showstatus;

  }
    getAssignmentQuesDetails(assignmentId){
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentQuestionsByAssignmentId/'+assignmentId );

    }
}
