import { Injectable } from '@angular/core';

import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../configuration/config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {


  constructor(private http: HttpClient, public configService: ConfigService) { }




  saveAssignmentDetails(assignmentData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignment', assignmentData);
  }

  getAllAssignmentDetails() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignment' );
  }

  updateAssignmentByQuestion(assignmentId,assignmentDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'updateQuestionsIdsForAssignment/'+assignmentId, assignmentDetails);
  }


  getAssignmentDetails(assignmentId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'getAssignment/'+assignmentId );
  }
  saveAssignmentGroupDetails(assignmentGroupData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'assignmentGroup', assignmentGroupData);
  }

  getAllAssignmentGroups() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentAllGroup');
  }
  updateAssignmentByGroupDetails(assignmentId,assignmentDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl  + 'updateQuestionsIdsForAssignment/'+assignmentId, assignmentDetails);
  }






}
