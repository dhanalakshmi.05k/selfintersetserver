import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentGroupService {
  public groupIdDetails:any
  constructor(private http: HttpClient, public configService: ApiConfigService) { }

  saveAssignmentGroupDetails(assignmentGroupData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentGroup', assignmentGroupData);
  }

  getAllAssignmentGroupDetails() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentAllGroup' );
  }
  getAssignmentGroupId(groupId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentGroupDetailsById/'+groupId );
  }

  getAssignmentGroupDetailsByAssignmentId(AssignmentId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'getAssignmentGroupDetailsByAssignmentId/'+AssignmentId );
  }

  setGroupIdDetails(groupIdList){
    this.groupIdDetails= groupIdList
  }
  getGroupIdDetails(){
    return this.groupIdDetails

  }


}
