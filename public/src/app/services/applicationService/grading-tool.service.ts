import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class GradingToolService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveGradingToolDetails(gradingToolDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addGradingTool', gradingToolDetails);
  }

  getAllGradingTools(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'gradingTools/'+ start+'/'+end);
  }

  getGradingToolByMongodbId(gradingToolId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'gradingToolById/' + gradingToolId);
  }

  updateGradingToolById(gradingToolId, gradingToolData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editGradingToolsById/' + gradingToolId, gradingToolData);
  }


  deleteGradingToolByMongodbId(gradingToolId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'gradingTool/' + gradingToolId);
  }

  getGradingToolCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingTool/count');
  }

}
