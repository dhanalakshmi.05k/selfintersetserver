import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class HabitsOfLearnerService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveHabitsOfLearnerDetails(habitsOfLearnerDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addHabitsOfLearners', habitsOfLearnerDetails);
  }

  getAllHabitsOfLearnerDetails(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'habitsOfLearners/'+ start+'/'+end);
  }

  getHabitsOfLearnerDetailsByMongodbId(habitsOfLearnerId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'habitsOfLearnerDetailsById/' + habitsOfLearnerId);
  }

  updateHabitsOfLearnerDetailsById(habitsOfLearnerId, habitsOfLearnerDetailsData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'habitsOfLearners/' + habitsOfLearnerId, habitsOfLearnerDetailsData);
  }


  deleteHabitsOfLearnerDetailsByMongodbId(habitsOfLearnerId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'habitsOfLearners/' + habitsOfLearnerId);
  }

  getHabitsOfLearnerDetailsCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allHabitsOfLearnerDetails/count');
  }

}
