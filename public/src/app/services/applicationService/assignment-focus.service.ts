import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentFocusService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentFocusDetails(assignmentFocusDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentFocus', assignmentFocusDetails);
  }

  getAllAssignmentFocusDetails(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentFocus/'+ start+'/'+end);
  }

  getAssignmentFocusDetailsByMongodbId(assignmentFocusId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentFocus/' + assignmentFocusId);
  }

  updateAssignmentFocusDetailsById(assignmentFocusId, assignmentFocusDetailsData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'assignmentFocus/' + assignmentFocusId, assignmentFocusDetailsData);
  }


  deleteAssignmentFocusDetailsByMongodbId(assignmentFocusId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'assignmentFocus/' + assignmentFocusId);
  }

  getAssignmentFocusDetailsCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentFocus/count');
  }

}
