import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentSplitBoardService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentSplitBoardDetails(assignmentSplitBoardDetail) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentSplitBoard', assignmentSplitBoardDetail);
  }

  getAllAssignmentSplitBoardDetail(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSplitBoard/'+start+'/'+end  );
  }

  getAssignmentSplitBoardDetailByMongodbId(splitBoardId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentSplitBoardById/' + splitBoardId);
  }

  updateAssignmentSplitBoardDetailById(splitBoardId, assignmentSplitBoardDetailData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentSplitBoardById/' + splitBoardId, assignmentSplitBoardDetailData);
  }


  deleteAssignmentSplitBoardDetailByMongodbId(splitBoardId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentSplitBoard/' + splitBoardId);
  }

  getAssignmentSplitBoardDetailCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSplitBoardDetail/count');
  }

}
