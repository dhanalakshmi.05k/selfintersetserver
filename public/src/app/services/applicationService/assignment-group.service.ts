import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentGroupService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentGroupDetails(assignmentGroupDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentGroup', assignmentGroupDetails);
  }

  getAllAssignmentGroup(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentGroup/'+ start
    +'/'+end );
  }

  getAssignmentGroupByMongodbId(assignmentId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentGroupById/' + assignmentId);
  }

  updateAssignmentGroupById(assignmentId, assignmentGroupData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentGroupById/' + assignmentId, assignmentGroupData);
  }


  deleteAssignmentGroupByMongodbId(assignmentId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentGroup/' + assignmentId);
  }

  getAssignmentGroupCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentGroup/count');
  }

}
