import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentReasonService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentReasonDetails(assignmentReasonDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentReason', assignmentReasonDetails);
  }

  getAllAssignmentReason(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentReason/'+ start +'/'+end  );
  }

  getAssignmentReasonByMongodbId(assignmentReasonId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentReasonById/' + assignmentReasonId);
  }

  updateAssignmentReasonById(assignmentReasonId, assignmentReasonData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentReasonById/' + assignmentReasonId, assignmentReasonData);
  }


  deleteAssignmentReasonByMongodbId(assignmentReasonId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentReason/' + assignmentReasonId);
  }

  getAssignmentReasonCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentReason/count');
  }

}
