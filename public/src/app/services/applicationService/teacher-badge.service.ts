import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class TeacherBadgeService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveTeacherBadgeDetails(teacherBadgeDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addTeacherBadge', teacherBadgeDetails);
  }

  getAllTeacherBadgeDetails(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allTeacherBadge/'+ start+'/'+end);
  }

  getTeacherBadgeDetailsByMongodbId(teacherBadgeId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentTeacherBadgeById/' + teacherBadgeId);
  }

  updateTeacherBadgeDetailsById(teacherBadgeId, teacherBadgeDetailsData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentTeacherBadgeById/' + teacherBadgeId, teacherBadgeDetailsData);
  }


  deleteTeacherBadgeDetailsByMongodbId(teacherBadgeId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentTeacherBadge/' + teacherBadgeId);
  }

  getTeacherBadgeDetailsCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allTeacherBadgeDetails/count');
  }

}
