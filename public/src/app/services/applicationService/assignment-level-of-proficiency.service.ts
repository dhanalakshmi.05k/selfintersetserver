import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentLevelOfProficiencyService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentLevelOfProficiencyDetails(assignmentLevelOfProficiencyDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentLevelOfProficiency', assignmentLevelOfProficiencyDetails);
  }

  getAllAssignmentLevelOfProficiency(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentLevelOfProficiency/'+start+'/'+end );
  }

  getAssignmentLevelOfProficiencyByMongodbId(assignmentLevelOfProficiencyId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentLevelOfProficiencyById/' + assignmentLevelOfProficiencyId);
  }

  updateAssignmentLevelOfProficiencyById(assignmentLevelOfProficiencyId, assignmentLevelOfProficiencyData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentLevelOfProficiencyById/' + assignmentLevelOfProficiencyId, assignmentLevelOfProficiencyData);
  }


  deleteAssignmentLevelOfProficiencyByMongodbId(assignmentLevelOfProficiencyId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentLevelOfProficiency/' + assignmentLevelOfProficiencyId);
  }

  getAssignmentLevelOfProficiencyCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentLevelOfProficiency/count');
  }

}
