import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";
@Injectable({
  providedIn: 'root'
})
export class AssignmentSkillDetailService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentSkillDetails(assignmentSkillDetail) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentSkillDetail', assignmentSkillDetail);
  }

  getAllAssignmentSkillDetail(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSkillDetail/'+start+'/'+end );
  }

  getAssignmentSkillDetailByMongodbId(assignmentSkillId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentSkillDetailById/' + assignmentSkillId);
  }

  updateAssignmentSkillDetailById(assignmentSkillId, assignmentSkillDetailData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentSkillDetailById/' + assignmentSkillId, assignmentSkillDetailData);
  }


  deleteAssignmentSkillDetailByMongodbId(assignmentSkillId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentSkillDetail/' + assignmentSkillId);
  }

  getAssignmentSkillDetailCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentSkillDetail/count');
  }

}
