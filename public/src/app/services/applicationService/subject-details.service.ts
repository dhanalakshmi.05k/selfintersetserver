import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class SubjectDetailsService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
   }

   saveSubjectDetails(subjectDetails) {
     return this.http.post(this.configService.appConfig.appBaseUrl + 'addsubjectDetails', subjectDetails);
   }

   getAllSubject(skip,limit) {
     return this.http.get(this.configService.appConfig.appBaseUrl + 'allSubjectDetails'+'/'+skip+'/'+limit );
   }

   getSubjectByMongodbId(subjectId) {
     return this.http.get(this.configService.appConfig.appBaseUrl + 'subjectDetailsById/' + subjectId);
   }

   updateSubjectById(subjectId, subjectData) {
     return this.http.post(this.configService.appConfig.appBaseUrl + 'editSubjectDetailsById/' + subjectId, subjectData);
   }


   deleteSubjectByMongodbId(subjectId) {
     return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteSubjectDetails/' + subjectId);
   }

   getSubjectCount() {
     return this.http.get(this.configService.appConfig.appBaseUrl + 'allSubject/count');
   }

 }
