import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentWeightService {

constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentWeightDetails(assignmentWeightDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentWeight', assignmentWeightDetails);
  }

  getAllAssignmentWeight(start,end) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentWeight/'+start+'/'+end);
  }

  getAssignmentWeightByMongodbId(assignmentWeightId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentWeightById/' + assignmentWeightId);
  }

  updateAssignmentWeightById(assignmentWeightId, assignmentWeightData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentWeightById/' + assignmentWeightId, assignmentWeightData);
  }


  deleteAssignmentWeightByMongodbId(assignmentWeightId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentWeight/' + assignmentWeightId);
  }

  getAssignmentWeightCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentWeight/count');
  }

}
