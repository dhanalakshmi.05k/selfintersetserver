import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "../configservice/api-config.service";

@Injectable({
  providedIn: 'root'
})
export class AssignmentCategoryService {

  constructor(private http: HttpClient, public configService: ApiConfigService) {
  }

  saveAssignmentCategoryDetails(assignmentCategoryDetails) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'addAssignmentCategory', assignmentCategoryDetails);
  }

  getAllAssignmentCategory(skip, limit) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentCategory'+'/'+skip+'/'+limit );
  }

  getAssignmentCategoryByMongodbId(assignmentCategoryId) {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'assignmentCategoryById/' + assignmentCategoryId);
  }

  updateAssignmentCategoryById(assignmentCategoryId, assignmentCategoryData) {
    return this.http.post(this.configService.appConfig.appBaseUrl + 'editAssignmentCategoryById/' + assignmentCategoryId, assignmentCategoryData);
  }


  deleteAssignmentCategoryByMongodbId(assignmentCategoryId) {
    return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssignmentCategory/' + assignmentCategoryId);
  }

  getAssignmentCategoryCount() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentCategory/count');
  }

}
