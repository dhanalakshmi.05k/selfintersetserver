import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApiConfigService} from "./api-config.service";

@Injectable({
  providedIn: 'root'
})
export class ApplicationConfigurationService {

  constructor(private http: HttpClient, public configService: ApiConfigService) { }
  getAllSubjects() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSubjectsForDropDown');
  }

  getAllAssignmentWeight() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentWeightingForDropDown');
  }
  getAllAssignmentBadge() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentBadgeForDropDown');
  }
  getAllAssignmentCategory() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentCategoryForDropDown');
  }
  getAllAssignmentType() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentTypeForDropDown');
  }


  getAllAssignmentLevelOfProficiency() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentLOPForDropDown');
  }
  getAllAssignmentSkillDetail() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSkillDetailsForDropDown');
  }
  getAllAssignmentSplitBoard() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSpiltDashboardForDropDown');
  }
  getAllAssignmentReason() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentResonsForDropDown');
  }
  getAllGradingType() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingTypesForDropDown');
  }

  getAllGradingTools() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allGradingToolsForDropDown');
  }

  getAllClassDetails() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allClassForDropDown');
  }

getAllassignmentEvaluation(){
  return this.http.get(this.configService.appConfig.appBaseUrl + 'allAssignmentEvaluationForDropDown');
}
  getAllSemester() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allSemesterForDropDown');
  }
  getAllYear() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allYearForDropDown');
  }
  getAllStudent() {
    return this.http.get(this.configService.appConfig.appBaseUrl + 'allStudent');
  }
}
