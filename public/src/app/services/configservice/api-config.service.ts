import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiConfigService {

  public appConfig = {
    appBaseUrl: 'http://localhost:5200/'
  };

  constructor() {
  }
}


