import { Component, OnInit } from '@angular/core';
import {UserManagementService} from "../../services/usermanagement/user-management.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  userCredentials:any={}
  constructor(private userManagementService:UserManagementService,private router: Router) { }
  loginError:any
  passwordLinkStatus:any
  ngOnInit() {
  }
  sendEmailLink(){
    this.loginError=""
    this.passwordLinkStatus=""

    console.log(this.userCredentials)

    this.userManagementService.getUserByEmailId(this.userCredentials.userName)
      .subscribe((data: any) => {
        console.log(data);
        if (data && data['userName']) {
          this.userManagementService.sendForgotPasswordReuest(this.userCredentials)
            .subscribe((data: any) => {
              console.log(data);
              this.passwordLinkStatus="PLease check Email  to reset password "
            });
        }

        else {
          this.loginError = "Invalid User Name or Password"
        }

      })

  }
  updateUser(){
    if(this.userCredentials.password===this.userCredentials.repeatPassword){
    this.userManagementService.getUserByEmailId(this.userCredentials.userName)
      .subscribe((data: any) => {
        console.log(data);
        if (data && data['userName']) {
          this.userManagementService.updateUserByEmailID(this.userCredentials.userName,this.userCredentials)
            .subscribe((data: any) => {
              console.log(data);
              this.router.navigate(['login']);
            });
        }

        else {
          this.loginError = "Invalid User Name or Password"
        }

      })
    }
    else{
      this.passwordLinkStatus = "Password mismatch"
    }
  }
}
