import { Component, OnInit } from '@angular/core';
import {UserManagementService} from "../../services/usermanagement/user-management.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
userCredentials:any={}
repeatPasswordError=""
  constructor(private userManagementService:UserManagementService,private router: Router) { }

  ngOnInit() {
  }
 updatePassword(){
    console.log(this.userCredentials)

    if(this.userCredentials.password===this.userCredentials.repeatPassword){
      this.userManagementService.saveUserDetails(this.userCredentials)
        .subscribe((data: any) => {
          console.log(data);
          this.router.navigate(['login']);
        });
    }
    else{
      this.repeatPasswordError="password mismatch"
    }

  }

}
