import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {CookieService} from "angular2-cookie/core";
import {UserManagementService} from "../../services/usermanagement/user-management.service";
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userCredentials:any={}
  loginError:any=""
  constructor(private router: Router,public _cookieService:CookieService,private userManagementService:UserManagementService) {
    if(_cookieService.get('remember')) {
      this.userCredentials.username=this._cookieService.get('username');
      this.userCredentials.password=this._cookieService.get('password');
      this.userCredentials.rememberme=this._cookieService.get('remember');
    }

  }


  navigateForgotPassword(){
    this.router.navigate(['forgotPassword']);
  }

  navigateSignUp(){
    this.router.navigate(['signUp']);
  }

  showPassword(){
    if ($("#password").attr("type" ) == "password") {
      $("#password").attr("type" , "text") 
  
    } else {
      $("#password").attr("type" , "password") 
    }
  }
 

  addClass(){
    console.log(this.userCredentials)

    this.loginError=""
    $(".btn-login").click(function(){      
      $(".btn-login").removeClass("btn-active")
      $(this).addClass("btn-active") 
       
    });

    if(this.userCredentials.userName==="admin@gmail.com" && this.userCredentials.password==="admin" ){


      this._cookieService.put('username',this.userCredentials.username);
      this._cookieService.put('password',this.userCredentials.password);
      this._cookieService.put('remember',this.userCredentials.rememberme);
      console.log("this._cookieService.get('remember')-----------")
      localStorage.setItem('currentUser', JSON.stringify({ user: this.userCredentials.userName}));

      this.router.navigate(['mainDashBoard']);
    }

    else{
      this.loginError="Invalid User Name or Password"
    }
  /*  else{


      this._cookieService.put('username',this.userCredentials.username);
      this._cookieService.put('password',this.userCredentials.password);
      this._cookieService.put('remember',this.userCredentials.rememberme);
      console.log("this._cookieService.get('remember')-----------")
      localStorage.setItem('currentUser', JSON.stringify({ user: this.userCredentials.userName}));

      this.router.navigate(['mainDashBoard']);

    /!*  this.userManagementService.getUserByEmailId(this.userCredentials.userName)
        .subscribe((data: any) => {
          console.log(data);
          if(data && data['userName'] && data['password']=== this.userCredentials.password){
            localStorage.setItem('currentUser', JSON.stringify({ user: this.userCredentials.userName}));
            this.router.navigate(['mainDashBoard']);
          }



        });*!/
    }*/
  }
  ngOnInit() {
 
  }

}
