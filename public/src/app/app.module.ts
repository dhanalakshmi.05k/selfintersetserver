import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FileUploadModule} from "ng2-file-upload";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FroalaEditorModule, FroalaViewModule} from "angular-froala-wysiwyg";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import { LoginComponent } from './loginModule/login/login.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { DashboardCartViewComponent } from './assignmentModule/dashboard-cart-view/dashboard-cart-view.component';
import { CreateassigmentpopupComponent } from './assignmentModule/createassigmentpopup/createassigmentpopup.component';
import { CreateassigmentpopQuizComponent } from './assignmentModule/createassigmentpop-quiz/createassigmentpop-quiz.component';
import { CreateassigmentComponent } from './assignmentModule/createassigment/createassigment.component';
import {AssigmentListViewComponent} from "./assignmentModule/assigment-list-view/assigment-list-view.component";
import {ConfigurationServiceService} from "./services/configuration/configuration-service.service";
import {ApplicationConfigurationService} from "./services/configservice/application-configuration.service";
import { AssignmentBadgeComponent } from './applicationSettings/assignment-badge/assignment-badge.component';
import { AssignmentCategoryComponent } from './applicationSettings/assignment-category/assignment-category.component';
import {ConfigService} from "./services/configuration/config.service";
import {AssignmentService} from "./services/assignmentService/assignment.service";
import { AssignmentReasonComponent } from './applicationSettings/assignment-reason/assignment-reason.component';
import { YearDetailsComponent } from './applicationSettings/year-details/year-details.component';
import { SemesterDetailsComponent } from './applicationSettings/semester-details/semester-details.component';
import { AssignmentLevelOfProficiencyComponent } from './applicationSettings/assignment-level-of-proficiency/assignment-level-of-proficiency.component';
import { AssignmentSkillDetailComponent } from './applicationSettings/assignment-skill-detail/assignment-skill-detail.component';
import { AssignmentSplitBoardComponent } from './applicationSettings/assignment-split-board/assignment-split-board.component';
import { AssignmentTypeComponent } from './applicationSettings/assignment-type/assignment-type.component';
import { AssignmentWeightComponent } from './applicationSettings/assignment-weight/assignment-weight.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { SettingsDashboardComponent } from './applicationSettings/settings-dashboard/settings-dashboard.component';
import { GradingToolComponent } from './applicationSettings/grading-tool/grading-tool.component';
import { CreateassigmentaddquestionComponent } from './assignmentModule/createassigmentaddquestion/createassigmentaddquestion.component';
import { GradingTypeComponent } from './applicationSettings/grading-type/grading-type.component';
import { TeacherBadgeComponent } from './applicationSettings/teacher-badge/teacher-badge.component';
import { HabitsOfLearnerComponent } from './applicationSettings/habits-of-learner/habits-of-learner.component';
import { AssignmentFocusComponent } from './applicationSettings/assignment-focus/assignment-focus.component';
import { CreateassigmentgroupComponent } from './assignmentModule/createassigmentgroup/createassigmentgroup.component';
import { CreategropPopUpComponent } from './assignmentModule/creategrop-pop-up/creategrop-pop-up.component';
import { AssignmentpublishComponent } from './assignmentModule/assignmentpublish/assignmentpublish.component';
import {CookieService} from "angular2-cookie/core";
import { ForgotPasswordComponent } from './loginModule/forgot-password/forgot-password.component';
import { SignUpComponent } from './loginModule/sign-up/sign-up.component';
import { ResetPasswordComponent } from './loginModule/reset-password/reset-password.component';
import {GrdFilterPipe} from "./utility/search-filter";
import {StudentDetailsService} from "./services/usermanagement/student-details.service";
import {AssignmentGroupService} from "./services/applicationService/assignment-group.service";
import {QuestionService} from "./services/assignmentService/question.service";
import { ClassDetailsComponent } from './applicationSettings/class-details/class-details.component';
import { SubjectDetailsComponent } from './applicationSettings/subject-details/subject-details.component';
import { UserManagementComponent } from './applicationSettings/user-management/user-management.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { TargertLearningComponent } from './assignmentModule/targert-learning/targert-learning.component';
import {AssignmentStudentService} from "./services/assignmentService/assignment-student.service";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
/*import {GrdFilterPipe} from "../../../randdproject/likecomments/src/app/services/search-filter";*/


// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SideBarComponent,
    TopNavComponent,
    DashboardCartViewComponent,
    AssigmentListViewComponent,
    CreateassigmentpopupComponent,
    CreateassigmentpopQuizComponent,
    CreateassigmentComponent,
    AssignmentBadgeComponent,
    AssignmentCategoryComponent,
    AssignmentReasonComponent,
    YearDetailsComponent,
    SemesterDetailsComponent,
    AssignmentLevelOfProficiencyComponent,
    AssignmentSkillDetailComponent,
    AssignmentSplitBoardComponent,
    AssignmentTypeComponent,
    AssignmentWeightComponent,
    SettingsDashboardComponent,
    GradingToolComponent,
    CreateassigmentaddquestionComponent,
    GradingToolComponent,
    GradingTypeComponent,
    TeacherBadgeComponent,
    HabitsOfLearnerComponent,
    AssignmentFocusComponent,
    CreateassigmentgroupComponent,
    CreategropPopUpComponent,
    AssignmentpublishComponent,
    ForgotPasswordComponent,
    SignUpComponent,
    ResetPasswordComponent,
    GrdFilterPipe,
    ClassDetailsComponent,
    SubjectDetailsComponent,
    UserManagementComponent,
    TargertLearningComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule,
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    FileUploadModule,
    CKEditorModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule
  ],
  providers: [ConfigService,
    ConfigurationServiceService,AssignmentStudentService,
    AssignmentService,ApplicationConfigurationService,CookieService,StudentDetailsService,AssignmentGroupService,
      QuestionService,{provide: LocationStrategy, useClass: HashLocationStrategy}],

  bootstrap: [AppComponent]
})
export class AppModule { }
